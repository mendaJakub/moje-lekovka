import React from 'react';
import App from './src/view/App';
import moment from 'moment';

import csLocale from 'moment/locale/cs';
import enLocale from 'moment/locale/en-gb';

moment.locale('cs', csLocale);
moment.locale('en', enLocale);
moment.locale('en-gb', enLocale);

export default class Index extends React.Component {
  render() {
    return (
      <App />
    );
  }
}
