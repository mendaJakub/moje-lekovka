import React, { ComponentType } from 'react';
import { TemplateProps } from '../templates/SignInTemplate';
import withGuestAccess from './withGuestAccess';
import withSignInLayout from './withSignInLayout';

export default (WrappedComponent: ComponentType<any>, templateProps?: TemplateProps) => {
  return withGuestAccess(withSignInLayout(WrappedComponent, templateProps));
};
