import React, { ComponentType } from 'react';
import PrivateScreen from '../templates/PrivateScreen';
import { NavigationScreenProps } from 'react-navigation';

export default (WrappedComponent: ComponentType<any>) => {
  return (props: NavigationScreenProps) => {
    return (
      <PrivateScreen {...props}>
        <WrappedComponent {...props} />
      </PrivateScreen>
    );
  };
};
