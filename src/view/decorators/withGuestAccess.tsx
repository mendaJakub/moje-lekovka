import React, { ComponentType } from 'react';
import GuestScreen from '../templates/GuestScreen';
import { NavigationScreenProps } from 'react-navigation';

export default (WrappedComponent: ComponentType<any>) => {
  return (props: NavigationScreenProps) => {
    return (
      <GuestScreen {...props}>
        <WrappedComponent {...props} />
      </GuestScreen>
    );
  };
};
