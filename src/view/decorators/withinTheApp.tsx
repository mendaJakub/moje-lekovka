import React, { ComponentType } from 'react';
import withDefaultLayout from './withDefaultLayout';
import { TemplateProps } from '../templates/DefaultTemplate';
import withAuthentication from './withAuthentication';

export default (WrappedComponent: ComponentType<any>, templateProps?: TemplateProps) => {
  return withAuthentication(withDefaultLayout(WrappedComponent, templateProps));
};
