import React, { ComponentType } from 'react';
import DefaultTemplate, { TemplateProps } from '../templates/DefaultTemplate';
import { NavigationScreenProps } from 'react-navigation';

export default (WrappedComponent: ComponentType<any>, templateProps?: TemplateProps) => {
  return (props: NavigationScreenProps) => {
    return (
      <DefaultTemplate navigationProps={props} {...templateProps}>
        <WrappedComponent {...props} />
      </DefaultTemplate>
    );
  };
};
