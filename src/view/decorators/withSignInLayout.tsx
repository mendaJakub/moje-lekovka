import React, { ComponentType } from 'react';
import SignInTemplate, { TemplateProps } from '../templates/SignInTemplate';
import { NavigationScreenProps } from 'react-navigation';

export default (WrappedComponent: ComponentType<any>, templateProps?: TemplateProps) => {
  return (props: NavigationScreenProps) => {
    return (
      <SignInTemplate navigationProps={props} {...templateProps}>
        <WrappedComponent {...props} />
      </SignInTemplate>
    );
  };
};
