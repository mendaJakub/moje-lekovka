import styled from 'styled-components/native';
import { Fab, Button } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

interface StyledFabProps extends StyledWithTheme {
  isDisabled?: boolean;
}

export const StyledFab = styled(Fab)`
  background-color: ${(props: StyledFabProps) => props.isDisabled ? props.theme.colors.lightGrey : props.theme.colors.primary};
`;
