import React, { Component } from 'react';
import { NavigationScreenProps } from 'react-navigation';
import { Content } from 'native-base';
import { OffsetContent } from '../styled';
import AlarmsEditingForm from '../../forms/AlarmsEditingForm';

interface Props extends NavigationScreenProps {
}

class AlarmsEditing extends Component<Props> {

  render() {
    const { navigation } = this.props;
    const medicationTakingId = navigation.getParam('medicationTakingId', null);

    return (
        <Content>
          <OffsetContent>
            {medicationTakingId ? <AlarmsEditingForm medicationTakingId={medicationTakingId}/> : null}
          </OffsetContent>
        </Content>
    );
  }
}

export default AlarmsEditing;
