import React, { Component } from 'react';
import Detail from '../../containers/MedicationDetail';
import { NavigationScreenProps } from 'react-navigation';
import { offlineAlert } from '../../../utils/connectionCheck';
import { Icon, Button, Content } from 'native-base';
import { StyledFab } from './styled';
import { connect } from 'react-redux';
import { deleteMyMedication } from '../../../state/modules/my/actions';
import Modal from '../../components/SubmissionModal';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import theme from '../../theme/DefaultTheme/definitions';
import { OffsetContent } from '../styled';
import { NetInfo } from 'react-native';

interface DispatchProps {
  deleteMyMedication: typeof deleteMyMedication;
}

interface Props extends NavigationScreenProps, DispatchProps, WithNamespaces {
}

interface State {
  fabsOpen: boolean;
  modalOpen: boolean;
  isConnected: boolean;
}

class MedicationDetail extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      fabsOpen: false,
      modalOpen: false,
      isConnected: false,
    };

    this.handleDeleteMedication = this.handleDeleteMedication.bind(this);
    this.handleConnectivityChange = this.handleConnectivityChange.bind(this);
  }

  async componentDidMount() {
    const isConnected = await NetInfo.isConnected.fetch();
    this.setState({ isConnected });
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  private handleConnectivityChange(isConnected) {
    this.setState({ isConnected });
  }

  private handleDeleteMedication() {
    const { navigation } = this.props;
    const medicationTakingId = navigation.getParam('medicationTakingId', null);

    this.props.deleteMyMedication(medicationTakingId);
    this.setState({ modalOpen: false });
  }

  render() {
    const { navigation, t } = this.props;
    const { fabsOpen, isConnected } = this.state;
    const medicationTakingId = navigation.getParam('medicationTakingId', null);

    return (
      <>
        <Content>
          <OffsetContent>
            {medicationTakingId ? <Detail medicationTakingId={medicationTakingId}/> : null}
          </OffsetContent>
        </Content>
        {medicationTakingId ? (
          <StyledFab
            position="bottomRight"
            direction="up"
            active={fabsOpen}
            onPress={isConnected ? () => this.setState((prevState: State) => ({ fabsOpen: !prevState.fabsOpen })) : offlineAlert}
            isDisabled={!isConnected}
          >
            <Icon name="menu"/>
            <Button
              danger
              style={{ backgroundColor: theme.colors.danger }}
              onPress={() => this.setState({ modalOpen: true })}
            >
              <Icon name="trash"/>
            </Button>
            <Button
              warning
              style={{ backgroundColor: theme.colors.warning }}
              onPress={() => navigation.navigate('ALARMS_EDITING', { medicationTakingId: medicationTakingId })}
            >
              <Icon name="alarm"/>
            </Button>
            <Button
              info
              style={{ backgroundColor: theme.colors.tertiary }}
              onPress={() => navigation.navigate('EDIT_MEDICATION', { medicationTakingId: medicationTakingId })}
            >
              <Icon name="create"/>
            </Button>
          </StyledFab>
        ) : null}
        <Modal
          modalOpen={this.state.modalOpen}
          onClose={() => this.setState({ modalOpen: false })}
          onSubmit={this.handleDeleteMedication}
          infoText={t('routes.medicationDetail.deletionText')}
          submitText={t('routes.medicationDetail.deletionSubmit')}
          cancelText={t('routes.medicationDetail.deletionCancel')}
        />
      </>
    );
  }
}

const Connected = connect(null, { deleteMyMedication })(MedicationDetail);
export default withNamespaces()(Connected);
