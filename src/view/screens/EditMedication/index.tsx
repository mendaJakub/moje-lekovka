import React, { Component } from 'react';
import { NavigationScreenProps } from 'react-navigation';
import NewMedicationForm from '../../forms/NewMedicationForm/index';
import { connect } from 'react-redux';
import { getMyMedication } from '../../../state/modules/my/actions';
import { OffsetContent } from '../styled';
import { Content } from 'native-base';

interface DispatchProps {
  getMyMedication: typeof getMyMedication;
}

interface Props extends NavigationScreenProps, DispatchProps {}

class EditMedication extends Component<Props> {
  componentDidMount() {
    const { navigation } = this.props;
    const editingId = navigation.getParam('medicationTakingId', null);

    if (!editingId) {
      navigation.goBack();
    }
  }

  render() {
    const { navigation } = this.props;
    const editingId = navigation.getParam('medicationTakingId', null);

    return (
      <Content>
        <OffsetContent>
          {Boolean(editingId) && <NewMedicationForm
            editingId={editingId}
          />}
        </OffsetContent>
      </Content>
    );
  }
}

export default connect(null, { getMyMedication })(EditMedication);
