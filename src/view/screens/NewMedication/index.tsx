import React, { Component } from 'react';
import { NavigationScreenProps } from 'react-navigation';
import NewMedicationForm from '../../forms/NewMedicationForm';
import { connect } from 'react-redux';
import { getMyMedication } from '../../../state/modules/my/actions';
import { OffsetContent } from '../styled';
import { Content } from 'native-base';

interface DispatchProps {
  getMyMedication: typeof getMyMedication;
}

interface Props extends NavigationScreenProps, DispatchProps {
}

class NewMedication extends Component<Props> {
  render() {
    return (
      <Content>
        <OffsetContent>
          <NewMedicationForm/>
        </OffsetContent>
      </Content>
    );
  }
}

export default connect(null, { getMyMedication })(NewMedication);
