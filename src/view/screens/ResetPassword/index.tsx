import React, { Component } from 'react';
import { TouchableHighlight, View } from 'react-native';
import ResetPasswordForm from '../../forms/ResetPasswordForm';
import { NavigationScreenProps } from 'react-navigation';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { Heading, Link, InfoBox } from './styled';

interface Props extends NavigationScreenProps, WithNamespaces {

}

class ResetPassword extends Component<Props> {
  render() {
    const { navigation, t } = this.props;

    return (
      <View>
        <Heading>{t('routes.resetPassword.title')}</Heading>
        <ResetPasswordForm/>
        <InfoBox>
          <TouchableHighlight underlayColor="transparent" onPress={() => navigation.navigate('SIGN_IN')}>
            <Link>{t('routes.resetPassword.goToSignIn')}</Link>
          </TouchableHighlight>
        </InfoBox>
      </View>
    );
  }
}

export default withNamespaces()(ResetPassword);
