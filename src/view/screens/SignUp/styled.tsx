import styled from 'styled-components/native';
import { Text } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

export const Heading = styled(Text)`
  font-size: 18px;
  text-align: center;
  font-weight: bold;
  margin-bottom: 10px;
`;

export const InfoText = styled(Text)`
`;

export const Link = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.primary};
`;

export const InfoBox = styled.View`
  display: flex;
  flex-direction: row;
`;
