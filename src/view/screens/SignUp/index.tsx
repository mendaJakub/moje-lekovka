import React, { Component } from 'react';
import { TouchableHighlight, View } from 'react-native';
import SignUpForm from '../../forms/SignUpForm';
import { NavigationScreenProps } from 'react-navigation';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { Heading, InfoText, Link, InfoBox } from './styled';

interface Props extends NavigationScreenProps, WithNamespaces {

}

class SignUp extends Component<Props> {
  private form: any;

  constructor(props: Props) {
    super(props);

    this.handleSubmitSuccess = this.handleSubmitSuccess.bind(this);
  }

  private handleSubmitSuccess() {
    const { navigation } = this.props;

    if (this.form) {
      this.form.closeModal();
    }
    navigation.replace('SIGN_IN');
  }

  render() {
    const { t } = this.props;

    return (
      <View>
        <Heading>{t('routes.signUp.title')}</Heading>
        <SignUpForm
          registerRef={(form: any) => this.form = form}
          onSubmitSuccess={this.handleSubmitSuccess}
        />
        <InfoBox>
          <InfoText>{t('routes.signUp.haveAccount')} </InfoText>
          <TouchableHighlight underlayColor="transparent" onPress={() => this.props.navigation.replace('SIGN_IN')}>
            <Link>{t('routes.signUp.goToSignIn')}</Link>
          </TouchableHighlight>
        </InfoBox>
      </View>
    );
  }
}

export default withNamespaces()(SignUp);
