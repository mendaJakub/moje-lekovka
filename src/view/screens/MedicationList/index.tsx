import React, { Component } from 'react';
import List from '../../containers/MedicationList';
import { NavigationScreenProps } from 'react-navigation';
import { OffsetContent } from '../styled';
import { Content } from 'native-base';

interface Props extends NavigationScreenProps {

}

export default class MedicationList extends Component<Props> {
  render() {
    return (
      <Content>
        <OffsetContent>
          <List {...this.props} />
        </OffsetContent>
      </Content>
    );
  }
}
