import React, { Component } from 'react';
import { TouchableHighlight, View } from 'react-native';
import SignInForm from '../../forms/SignInForm';
import { NavigationScreenProps } from 'react-navigation';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { Heading, InfoText, Link, InfoBox } from './styled';

interface Props extends NavigationScreenProps, WithNamespaces {

}

class SignIn extends Component<Props> {
  render() {
    const { navigation, t } = this.props;

    return (
      <View>
        <Heading>{t('routes.signIn.title')}</Heading>
        <SignInForm
          onSubmitSuccess={() => { navigation.replace('MEDICATION_LIST'); }}
        />
        <InfoBox>
          <InfoText>{t('routes.signIn.noAccount')} </InfoText>
          <TouchableHighlight underlayColor="transparent" onPress={() => navigation.navigate('SIGN_UP')}>
            <Link>{t('routes.signIn.goToSignUp')}</Link>
          </TouchableHighlight>
        </InfoBox>
        <InfoBox>
          <InfoText>{t('routes.signIn.forgottenPassword')} </InfoText>
          <TouchableHighlight underlayColor="transparent" onPress={() => navigation.navigate('RESET_PASSWORD')}>
            <Link>{t('routes.signIn.goToResetPassword')}</Link>
          </TouchableHighlight>
        </InfoBox>
      </View>
    );
  }
}

export default withNamespaces()(SignIn);
