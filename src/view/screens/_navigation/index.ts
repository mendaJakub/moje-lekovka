import { createStackNavigator, createAppContainer } from 'react-navigation';
import MedicationList from '../MedicationList';
import MedicationDetail from '../MedicationDetail';
import NewMedication from '../NewMedication';
import EditMedication from '../EditMedication';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import ResetPassword from '../ResetPassword';
import withinTheApp from '../../decorators/withinTheApp';
import outsideTheApp from '../../decorators/outsideTheApp';
import AlarmsEditing from '../AlarmsEditing';

export const Screens = {
  MEDICATION_LIST: 'MEDICATION_LIST',
  MEDICATION_DETAIL: 'MEDICATION_DETAIL',
  NEW_MEDICATION: 'NEW_MEDICATION',
  EDIT_MEDICATION: 'EDIT_MEDICATION',
  SIGN_IN: 'SIGN_IN',
  SIGN_UP: 'SIGN_UP',
  RESET_PASSWORD: 'RESET_PASSWORD',
  ALARMS_EDITING: 'ALARMS_EDITING',
};

const Navigation = createStackNavigator({
  [Screens.MEDICATION_LIST]: {
    screen: withinTheApp(MedicationList, {
      titleTranslationKey: 'routes.medicationList.title',
      fabSettings: {
        icon: 'add',
        navigateTo: Screens.NEW_MEDICATION,
        offlineDisabled: true,
      },
    }),
  },
  [Screens.MEDICATION_DETAIL]: {
    screen: withinTheApp(MedicationDetail, { titleTranslationKey: 'routes.medicationDetail.title'}),
  },
  [Screens.NEW_MEDICATION]: {
    screen: withinTheApp(NewMedication, { titleTranslationKey: 'routes.newMedication.title'}),
  },
  [Screens.EDIT_MEDICATION]: {
    screen: withinTheApp(EditMedication, { titleTranslationKey: 'routes.editMedication.title'}),
  },
  [Screens.ALARMS_EDITING]: {
    screen: withinTheApp(AlarmsEditing, { titleTranslationKey: 'routes.alarmsEditing.title'}),
  },
  [Screens.SIGN_IN]: {
    screen: outsideTheApp(SignIn, { titleTranslationKey: 'routes.signIn.title' }),
  },
  [Screens.SIGN_UP]: {
    screen: outsideTheApp(SignUp, { titleTranslationKey: 'routes.signUp.title' }),
  },
  [Screens.RESET_PASSWORD]: {
    screen: outsideTheApp(ResetPassword, { titleTranslationKey: 'routes.signIn.title' }),
  },
}, { headerMode: 'none' });

export default createAppContainer(Navigation);
