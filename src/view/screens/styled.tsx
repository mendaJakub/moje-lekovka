import styled from 'styled-components';
import { Content } from 'native-base';

export const OffsetContent = styled(Content)`
  margin: 10px;
  flex: 1;
  min-height: 500px;
`;
