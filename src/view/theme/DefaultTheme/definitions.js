export default {
  colors: {
    primary: '#6CBB41',
    secondary: '#3A4C02',
    tertiary: '#5067FF',
    lightGrey: '#cdcdcd',
    lighterGrey: '#E8E8E8',
    dirtyWhite: '#F7F7F7',
    danger: '#d9534f',
    warning: '#EB9E3E',
  },
};
