import definitions from './definitions';

export interface StyledWithTheme {
  theme?: {
    colors: {
      primary: string;
      secondary: string;
      tertiary: string;
      lightGrey: string;
      lighterGrey: string;
      dirtyWhite: string;
      danger: string;
      warning: string;
    };
  };
}

export default definitions;
