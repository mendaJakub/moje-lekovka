import React, { Component } from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { StyleProvider, Toast, Root } from 'native-base';
import getTheme from './theme/native-base-theme/components';
import platform from './theme/native-base-theme/variables/platform';
import { I18nextProvider } from 'react-i18next';
import { AppLoading, Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import configureStore from './../state/configureStore';
import theme from './theme/DefaultTheme/index';
import Navigation from './screens/_navigation';
import resolveI18next from '../lib/i18n';
import NavigationService from '../utils/NavigationService';

interface Props {
}

interface State {
  isReady: boolean;
}

const { store } = configureStore();

export default class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
      Entypo: require('native-base/Fonts/Entypo.ttf'),
      Feather: require('native-base/Fonts/Feather.ttf'),
      FontAwesome: require('native-base/Fonts/FontAwesome.ttf'),
      Octicons: require('native-base/Fonts/Octicons.ttf'),
    });
    this.setState({ isReady: true });
  }

  render() {
    const { isReady } = this.state;

    if (!isReady) {
      return <AppLoading/>;
    }

    return (
      <ReduxProvider store={store}>
        <I18nextProvider i18n={resolveI18next(store.dispatch)}>
          <StyleProvider style={getTheme(platform)}>
            <ThemeProvider theme={theme}>
              <Root>
                <Navigation
                  ref={(navigationRef: any) => NavigationService.setTopLevelNavigator(navigationRef)}
                />
              </Root>
            </ThemeProvider>
          </StyleProvider>
        </I18nextProvider>
      </ReduxProvider>
    );
  }
}
