import React, { Component } from 'react';
import { ReduxStateType } from '../../../state/rootReducer';
import { connect } from 'react-redux';
import { NavigationScreenProps } from 'react-navigation';
import { selectLoading, selectToken } from '../../../state/modules/auth/selectors';
import Loader from '../../components/Loader';
import { refreshToken, retrieveTokenFromStorage } from '../../../state/modules/auth/actions';
import { Token } from '../../../lib/ApiService/users/types';
import moment from 'moment';

const TOKEN_EXPIRATION_LIMIT_DAYS = 15;

interface Props extends NavigationScreenProps {
  authorization: Token;
  loading: boolean;
  retrieveTokenFromStorage: typeof retrieveTokenFromStorage;
  refreshToken: typeof refreshToken;
}

class PrivateScreen extends Component<Props> {
  componentDidMount() {
    const { authorization } = this.props;

    if (!authorization) {
      this.props.retrieveTokenFromStorage();
    } else {
      this.refreshIfNeeded();
    }
  }

  componentDidUpdate(prevProps: Props) {
    const { authorization, loading, navigation } = this.props;

    if (!loading) {
      if (!authorization) {
        navigation.replace('SIGN_UP');
      } else {
        this.refreshIfNeeded();
      }
    }
  }

  private refreshIfNeeded() {
    const { authorization } = this.props;

    const diff = moment(authorization.expirationDate).diff(moment(), 'days');
    if (diff < TOKEN_EXPIRATION_LIMIT_DAYS) {
      this.props.refreshToken();
    }
  }

  render() {
    const { authorization, loading, children } = this.props;

    if (!authorization || loading) {
      return <Loader />;
    }

    return children;
  }
}

function mapStateToProps(state: ReduxStateType) {
  return {
    authorization: selectToken(state),
    loading: selectLoading(state),
  };
}

export default connect(mapStateToProps, { retrieveTokenFromStorage, refreshToken })(PrivateScreen);
