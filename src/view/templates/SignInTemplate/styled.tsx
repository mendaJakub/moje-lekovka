import styled from 'styled-components/native';
import { StyledWithTheme } from '../../theme/DefaultTheme';

interface StatusBarProps extends StyledWithTheme {
  height: number;
}

export const StatusBarPlaceholder = styled.View`
  background-color: #fff;
  height: ${(props: StatusBarProps) => `${props.height}px`};
  width: 100%;
`;

export const OffsetContent = styled.View`
  padding: 20% 10% 10%;
`;

export const FormContainer = styled.View`
  flex: 1;
`;

export const Logo = styled.Image`
  flex: 1;
  align-self: center;
  width: 200px;
  height: 82px;
  margin-bottom: 20px;
`;
