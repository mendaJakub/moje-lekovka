import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import { StatusBarPlaceholder, OffsetContent, FormContainer, Logo } from './styled';
import { StatusBar } from 'react-native';
import { withNamespaces, WithNamespaces } from 'react-i18next';

export interface TemplateProps {
  titleTranslationKey: string;
}

interface Props extends TemplateProps, WithNamespaces {
  navigationProps: NavigationScreenProps;
}

class SignInTemplate extends Component<Props> {
  render() {
    const { children } = this.props;

    return (
        <Container>
          <StatusBarPlaceholder height={StatusBar.currentHeight || 18} />
          <Content>
            <OffsetContent>
              <Logo
                resizeMode="contain"
                source={require('../../../../assets/logo.png')}
              />
              <FormContainer>
                {children}
              </FormContainer>
            </OffsetContent>
          </Content>
        </Container>
    );
  }
}

export default withNamespaces()(SignInTemplate);
