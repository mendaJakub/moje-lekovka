import React, { Component } from 'react';
import { ReduxStateType } from '../../../state/rootReducer';
import { connect } from 'react-redux';
import { NavigationScreenProps } from 'react-navigation';

interface Props extends NavigationScreenProps {
  authorization: string;
}

class GuestScreen extends Component<Props> {
  render() {
    const { authorization, navigation, children } = this.props;

    if (authorization) {
      navigation.navigate('MEDICATION_LIST');
    }

    return children;
  }
}

function mapStateToProps(state: ReduxStateType) {
  return {
    authorization: null,
  };
}

export default connect(mapStateToProps)(GuestScreen);
