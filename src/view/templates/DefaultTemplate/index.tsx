import React, { Component, ReactElement } from 'react';
import {
  Container,
  Header,
  Title,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Toast,
} from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import { StatusBarPlaceholder, OffsetContainer, StyledFab } from './styled';
import { Alert, NetInfo, StatusBar } from 'react-native';
import PrimaryNavigation from '../../containers/MainNavigation';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { offlineAlert } from '../../../utils/connectionCheck';
import { Notifications } from 'expo';
import { getiOSNotificationPermission, setUpAndroidNotificationChannels } from '../../../utils/notifications';
import { EventSubscription } from 'fbemitter';

export interface TemplateProps {
  titleTranslationKey: string;
  fabSettings?: {
    icon: string;
    navigateTo: string;
    children?: ReactElement[];
    offlineDisabled?: boolean;
  };
}

interface Props extends TemplateProps, WithNamespaces {
  navigationProps: NavigationScreenProps;
}

interface State {
  isConnected: boolean;
}

class DefaultTemplate extends Component<Props, State> {
  private navigation: any;
  private listener: EventSubscription;

  constructor(props: Props) {
    super(props);

    this.state = {
      isConnected: false,
    };

    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
    this.handleFabPress = this.handleFabPress.bind(this);
    this.handleConnectivityChange = this.handleConnectivityChange.bind(this);
  }

  async componentDidMount() {
    getiOSNotificationPermission();
    setUpAndroidNotificationChannels();
    this.listenForNotifications();
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);

    const isConnected = await NetInfo.isConnected.fetch();
    this.setState({ isConnected });
  }

  componentWillUnmount() {
    this.listener.remove();
    this.listener = null;

    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  private handleOpenDrawer() {
    this.navigation.openDrawer();
  }

  private async handleFabPress(navigateTo: string, offlineDisabled: boolean) {
    const { navigationProps, t } = this.props;
    const { isConnected } = this.state;

    if (offlineDisabled && !isConnected) {
      offlineAlert();
    } else {
      navigationProps.navigation.navigate(navigateTo);
    }
  }

  private listenForNotifications() {
    this.listener = Notifications.addListener((notification: any) => {
      if (notification.origin === 'received') {
        Alert.alert(notification.data.title, notification.data.body);
      }
    });
  }

  private handleConnectivityChange(isConnected) {
    const { t } = this.props;

    this.setState({ isConnected });

    if (!isConnected) {
      Toast.show({
        text: t('flashes.netConnectionRestriction'),
        duration: 4000,
        buttonText: t('flashes.ok'),
      });
    }
  }

  render() {
    const { fabSettings, children, navigationProps, titleTranslationKey, t } = this.props;
    const { isConnected } = this.state;

    return (
      <PrimaryNavigation registerRef={(el: any) => this.navigation = el} {...navigationProps}>
        <Container>
          <StatusBarPlaceholder height={StatusBar.currentHeight || 18} />
          <Header>
            <Left>
              <Button transparent onPress={this.handleOpenDrawer}>
                <Icon name="menu"/>
              </Button>
            </Left>
            <Body>
            <Title>{t(titleTranslationKey)}</Title>
            </Body>
            <Right/>
          </Header>
          <OffsetContainer>
            {children}
          </OffsetContainer>
          {fabSettings && <StyledFab
            position="bottomRight"
            onPress={() => this.handleFabPress(fabSettings.navigateTo, fabSettings.offlineDisabled)}
            isDisabled={fabSettings.offlineDisabled && !isConnected}
          >
            <Icon name={fabSettings.icon} />
            {fabSettings.children || null}
          </StyledFab>}
        </Container>
      </PrimaryNavigation>
    );
  }
}

export default withNamespaces()(DefaultTemplate);
