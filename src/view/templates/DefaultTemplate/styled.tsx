import styled from 'styled-components/native';
import { Platform } from 'react-native';
import { StyledWithTheme } from '../../theme/DefaultTheme';
import { Fab } from 'native-base';

interface StatusBarProps extends StyledWithTheme {
  height: number;
}

interface StyledFabProps extends StyledWithTheme {
  isDisabled?: boolean;
}

export const StatusBarPlaceholder = styled.View`
  background-color: ${(props: StatusBarProps) => Platform.OS === 'ios' ? '#F8F8F8' : props.theme.colors.primary};
  height: ${(props: StatusBarProps) => `${props.height}px`};
  width: 100%;
`;

export const OffsetContainer = styled.View`
  flex: 1;
`;

export const StyledFab = styled(Fab)`
  background-color: ${(props: StyledFabProps) => props.isDisabled ? props.theme.colors.lightGrey : props.theme.colors.primary};
`;
