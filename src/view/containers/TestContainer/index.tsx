import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { testRedux } from '../../../state/modules/test/actions';

interface Props {
  testRedux: () => void;
}

class TestContainer extends Component<Props> {
  componentDidMount() {
    this.props.testRedux();
  }

  render() {
    return (
      <View><Text>Test container</Text></View>
    );
  }
}

export default connect(null, { testRedux })(TestContainer);
