import React, { Component } from 'react';
import { getMyMedicationDetail } from '../../../state/modules/my/actions';
import { Medication } from '../../../lib/ApiService/medication/types';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { ReduxStateType } from '../../../state/rootReducer';
import {
  selectMyMedicationDetail,
  selectMyMedicationDetailError,
  selectMyMedicationDetailLoading, selectMyMedicationDetailRoot,
} from '../../../state/modules/my/selectors';
import { connect, MapStateToProps } from 'react-redux';
import theme from '../../theme/DefaultTheme/definitions';
import { Spinner } from 'native-base';
import {
  ErrorText,
  LoaderWrapper,
  MessageWrapper,
  ErrorIcon,
  Title,
  StyledIcon,
  TitleWrapper,
  Wrapper,
  ErrorMessage,
} from './styled';
import MedicationInfo from '../../components/MedicationInfo';
import { MedicationTakingItem } from '../../../state/modules/my/@types';

interface ComponentProps {
  medicationTakingId: string;
}

interface StateProps {
  data: Medication;
  loading: boolean;
  error: Error;
  medication: MedicationTakingItem;
}

interface DispatchProps {
  getMyMedicationDetail: typeof getMyMedicationDetail;
}

interface Translated extends ComponentProps, WithNamespaces {}

interface Props extends StateProps, DispatchProps, Translated {}

class MedicationDetail extends Component<Props> {
  componentDidMount() {
    const { medicationTakingId } = this.props;

    this.props.getMyMedicationDetail(medicationTakingId);
  }

  render() {
    const { loading, data, error, medication, t } = this.props;

    if (error) {
      return (
        <MessageWrapper>
          <ErrorIcon name="warning" />
          <ErrorText>{t('containers.MedicationDetail.error')}</ErrorText>
        </MessageWrapper>
      );
    }
    if (loading) {
      return (
        <LoaderWrapper>
          <Spinner color={theme.colors.primary} />
        </LoaderWrapper>
      );
    }

    if (!data) {
      return (
        <ErrorMessage>{t('containers.MedicationDetail.noContent')}</ErrorMessage>
      );
    }

    return (
      <Wrapper>
        <TitleWrapper>
          <StyledIcon name="medkit" />
          <Title>{medication.title}</Title>
        </TitleWrapper>
        <MedicationInfo medication={data} />
      </Wrapper>
    );
  }
}

const mapStateToProps: MapStateToProps<StateProps, Translated, ReduxStateType> = (state: ReduxStateType, props: Translated) => {
  return {
    data: selectMyMedicationDetail(props.medicationTakingId)(state),
    medication: selectMyMedicationDetailRoot(props.medicationTakingId)(state),
    loading: selectMyMedicationDetailLoading(props.medicationTakingId)(state),
    error: selectMyMedicationDetailError(props.medicationTakingId)(state),
  };
};

const Connected = connect(mapStateToProps, { getMyMedicationDetail })(MedicationDetail);
export default withNamespaces()(Connected);
