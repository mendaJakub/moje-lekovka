import styled from 'styled-components/native';
import { Icon, Text } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

export const Wrapper = styled.View`
  padding-bottom: 40px;
`;

export const ErrorIcon = styled(Icon)`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
  margin-bottom: 20px;
`;

export const ErrorText = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
`;

export const ErrorMessage = styled(Text)`
  text-align: center;
  margin: 20px auto;
  padding: 0 20px;
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;

export const LoaderWrapper = styled.View`
  padding: 40px 0;
`;

export const MessageWrapper = styled.View`
  padding: 40px 0;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const TitleWrapper = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 30px 10px;
  border-color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
  border-bottom-width: 1px;
  margin-bottom: 20px;
`;

export const Title = styled(Text)`
  font-size: 24px;
  text-align: center;
  color: ${(props: StyledWithTheme) => props.theme.colors.secondary};
`;

export const StyledIcon = styled(Icon)`
  color: ${(props: StyledWithTheme) => props.theme.colors.secondary};
  margin-right: 10px;
`;
