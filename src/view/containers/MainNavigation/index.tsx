import React, { Component } from 'react';
import { NavigationScreenProps } from 'react-navigation';
import { Drawer, Container, List, ListItem, Text, Button, Icon } from 'native-base';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { Content, Header, Logo, NavContent, StyledIcon } from './styled';
import { connect } from 'react-redux';
import { signOut } from '../../../state/modules/auth/actions';

interface ComponentProps extends NavigationScreenProps, WithNamespaces {
  registerRef: (el: any) => void;
}

interface DispatchProps {
  signOut: typeof signOut;
}

interface Props extends ComponentProps, DispatchProps {
}

interface RouteItem {
  id: string;
  translationKey: string;
  icon: string;
}

const ROUTES = [
  {
    id: 'MEDICATION_LIST',
    translationKey: 'routes.medicationList.title',
    icon: 'list',
  },
];

class MainNavigation extends Component<Props> {
  private drawer: any;

  constructor(props: Props) {
    super(props);

    this.renderRoute = this.renderRoute.bind(this);
    this.renderMenu = this.renderMenu.bind(this);
  }

  componentDidMount() {
    this.props.registerRef(this);
  }

  public openDrawer() {
    this.drawer._root.open();
  }

  public closeDrawer() {
    this.drawer._root.close();
  }

  renderRoute(item: RouteItem) {
    const { t } = this.props;

    return (
      <ListItem
        button
        onPress={() => this.props.navigation.replace(item.id)}
      >
        <StyledIcon name={item.icon} />
        <Text>{t(item.translationKey)}</Text>
      </ListItem>
    );
  }

  renderMenu() {
    const { t } = this.props;

    return (
      <Container>
        <Content>
          <Header>
            <Logo
              resizeMode="contain"
              source={require('../../../../assets/logo-inverse.png')}
            />
          </Header>
          <NavContent>
            <List
              dataArray={ROUTES}
              renderRow={this.renderRoute}
            />
            <Button/>
          </NavContent>
          <Button
            full
            iconLeft
            transparent
            onPress={() => this.props.signOut()}
          >
            <Icon name="log-out" />
            <Text>{t('containers.MainNavigation.signOut')}</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  render() {
    const { children } = this.props;

    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={this.renderMenu()}
        onClose={() => this.closeDrawer()}>
        {children}
      </Drawer>
    );
  }
}

const Connected = connect(null, { signOut })(MainNavigation);
export default withNamespaces()(Connected);
