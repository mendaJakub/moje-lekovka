import styled from 'styled-components/native';
import { Icon } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

export const Header = styled.View`
  background: ${(props: StyledWithTheme) => props.theme.colors.primary};
  width: 100%;
  flex: 1;
  justify-content: center;
  padding-top: 18px;
  padding-left: 20px;
`;

export const Logo = styled.Image`
  display: flex;
  width: 200px;
  height: 82px;
`;

export const NavContent = styled.View`
  flex: 3;
`;

export const Content = styled.View`
  flex: 1;
`;

export const StyledIcon = styled(Icon)`
  margin-right: 20px;
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;
