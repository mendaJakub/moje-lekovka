import styled from 'styled-components/native';
import { Icon, Text } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

export const StyledIcon = styled(Icon)`
  margin-right: 20px;
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;

export const NoContentIcon = styled(Icon)`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
  margin-bottom: 20px;
`;

export const ErrorIcon = styled(Icon)`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
  margin-bottom: 20px;
`;

export const ErrorText = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
`;

export const NoContentText = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;

export const LoaderWrapper = styled.View`
  padding: 40px 0;
`;

export const MessageWrapper = styled.View`
  padding: 40px 0;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
