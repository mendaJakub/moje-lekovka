import React, { Component } from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { getMyMedication } from '../../../state/modules/my/actions';
import { ReduxStateType } from '../../../state/rootReducer';
import {
  selectMyMedication,
  selectMyMedicationError,
  selectMyMedicationLoading,
} from '../../../state/modules/my/selectors';
import { List, ListItem, Text, Spinner } from 'native-base';
import {
  StyledIcon,
  ErrorText,
  LoaderWrapper,
  NoContentText,
  MessageWrapper,
  NoContentIcon,
  ErrorIcon,
} from './styled';
import theme from '../../theme/DefaultTheme/definitions';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { NavigationScreenProps } from 'react-navigation';
import { MedicationTakingItem } from '../../../state/modules/my/@types';

interface ComponentProps extends NavigationScreenProps {
}

interface StateProps {
  data: MedicationTakingItem[];
  loading: boolean;
  error: Error;
}

interface DispatchProps {
  getMyMedication: typeof getMyMedication;
}

interface Translated extends ComponentProps, WithNamespaces {
}

interface Props extends StateProps, DispatchProps, Translated {
}

class MedicationList extends Component<Props> {
  constructor(props: Props) {
    super(props);

    this.renderRoute = this.renderRoute.bind(this);
  }

  componentDidMount() {
    this.props.getMyMedication();
  }

  componentDidUpdate() {
    const { data, loading } = this.props;

    if (!data && !loading) {
      this.props.getMyMedication();
    }
  }

  renderRoute(item: MedicationTakingItem) {
    const { navigation } = this.props;

    return (
      <ListItem
        button
        onPress={() => navigation.navigate('MEDICATION_DETAIL', { medicationTakingId: item._id })}
      >
        <StyledIcon name="medkit"/>
        <Text>{item.title}</Text>
      </ListItem>
    );
  }

  render() {
    const { loading, data, error, t } = this.props;

    if (error) {
      return (
        <MessageWrapper>
          <ErrorIcon name="warning"/>
          <ErrorText>{t('containers.MedicationList.error')}</ErrorText>
        </MessageWrapper>
      );
    }
    if (loading) {
      return (
        <LoaderWrapper>
          <Spinner color={theme.colors.primary}/>
        </LoaderWrapper>
      );
    }

    if (!data) {
      return null;
    }

    if (!data.length) {
      return (
        <MessageWrapper>
          <NoContentIcon name="medkit"/>
          <NoContentText>{t('containers.MedicationList.noContent')}</NoContentText>
        </MessageWrapper>
      );
    }

    return (
      <>
        <List
          dataArray={data}
          renderRow={this.renderRoute}
        />
      </>
    );
  }
}

const mapStateToProps: MapStateToProps<StateProps, Translated, ReduxStateType> = (state: ReduxStateType) => {
  return {
    data: selectMyMedication(state),
    loading: selectMyMedicationLoading(state),
    error: selectMyMedicationError(state),
  };
};

const Connected = connect(mapStateToProps, { getMyMedication })(MedicationList);
export default withNamespaces()(Connected);
