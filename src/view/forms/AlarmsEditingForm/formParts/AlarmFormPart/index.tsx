import React, { Component } from 'react';
import { View } from 'react-native';
import {
  ButtonWrapper,
  FieldsWrapper,
  Wrapper,
  MessageWrapper,
  AlarmContent,
  AlarmContentSection,
  AddNew,
  AlarmHeading,
  AlarmWrapper,
  DeleteButtonWrapper,
  HeadingIcon,
  Header,
} from '../../styled';
import { Button, Icon } from 'native-base';
import { WrappedFieldArrayProps } from 'redux-form';
import { NamespacesConsumer } from 'react-i18next';
import CheckboxGroup from '../../../../components/redux-form-bindings/CheckboxGroup';
import TimePickerField from '../../../../components/redux-form-bindings/TimePicker';
import { daysAWeek } from './constants';
import _ from 'lodash';
import { arrayRequired } from '../../../../../utils/validation';
import { connect } from 'react-redux';
import { ReduxStateType } from '../../../../../state/rootReducer';
import { selectItemField } from '../../../../../state/modules/my/selectors';
import Modal from '../../../../components/SubmissionModal';
import { deleteMyMedicationAlarm } from '../../../../../state/modules/my/actions';
import { Alarm, NotificationTypes } from '../../../../../lib/ApiService/medication/types.d';

const emptyAlarm = {
  days: [],
  timesOfDay: [],
  notificationTypes: [NotificationTypes.PUSH_NOTIFICATION],
};

interface State {
  openModalKey: string;
  openDeletionModalKey: string;
}

interface StateProps {
  selectItem: (itemKey: string) => any;
}

interface DispatchProps {
  deleteMyMedicationAlarm: typeof deleteMyMedicationAlarm;
}

interface ComponentProps {
  medicationId: string;
}

interface Props extends ComponentProps, StateProps, DispatchProps, WrappedFieldArrayProps<Alarm> {}

class AlarmFormPart extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      openModalKey: null,
      openDeletionModalKey: null,
    };

    this.handleDeleteAlarm = this.handleDeleteAlarm.bind(this);
  }

  private handleDeleteAlarm(item: string, index: number) {
    const { fields, medicationId } = this.props;
    const selectedItem = this.props.selectItem(item);

    if (selectedItem._id) {
      this.props.deleteMyMedicationAlarm(medicationId, selectedItem._id);
    }

    fields.remove(index);
    this.setState({ openDeletionModalKey: null });
  }

  render() {
    const { fields, meta: { error, submitFailed } } = this.props;
    const { openModalKey, openDeletionModalKey } = this.state;

    return (
      <NamespacesConsumer ns={null}>
        {(t) => (
          <Wrapper>
            <FieldsWrapper>
              {fields.map((item: string, index: number) => (
                <View key={index}>
                  <Header>
                    <HeadingIcon name="alarm" />
                    <AlarmHeading>{t('forms.AlarmsEditing.alarmHeading', { index: index + 1 })}</AlarmHeading>
                  </Header>
                  <AlarmWrapper>
                    <AlarmContent>
                      <AlarmContentSection>
                        <CheckboxGroup
                          name={`${item}.days`}
                          label={t('forms.AlarmsEditing.days')}
                          options={daysAWeek.map((day: { value: number; label: string }) => ({
                            ...day,
                            label: t(day.label),
                          }))}
                          error={(submitFailed && error) ? _.get(error, 'days') : undefined}
                          validate={arrayRequired}
                          isOpen={openModalKey === item}
                          setOpen={() => this.setState({ openModalKey: item })}
                          onClose={() => this.setState({ openModalKey: null })}
                          openModalText={t('forms.AlarmsEditing.openModal')}
                          dismissModalText={t('forms.AlarmsEditing.modalDismiss')}
                        />
                      </AlarmContentSection>
                      <AlarmContentSection>
                        <TimePickerField
                          name={`${item}.timesOfDay`}
                          label={t('forms.AlarmsEditing.timesOfDay')}
                          error={(submitFailed && error) ? _.get(error, 'timesOfDay') : undefined}
                          validate={arrayRequired}
                        />
                      </AlarmContentSection>
                    </AlarmContent>
                    <DeleteButtonWrapper>
                      <Button
                        dark
                        transparent
                        onPress={() => this.setState({ openDeletionModalKey: item })}
                      >
                        <Icon name="close"/>
                      </Button>
                    </DeleteButtonWrapper>
                  </AlarmWrapper>
                  <Modal
                    modalOpen={openDeletionModalKey === item}
                    onClose={() => this.setState({ openDeletionModalKey: null })}
                    onSubmit={() => this.handleDeleteAlarm(item, index)}
                    infoText={t('forms.AlarmsEditing.deletionText')}
                    submitText={t('forms.AlarmsEditing.deletionSubmit')}
                    cancelText={t('forms.AlarmsEditing.deletionCancel')}
                  />
                </View>
              ))}
            </FieldsWrapper>
            <AddNew onPress={() => fields.push(emptyAlarm)}>
              <MessageWrapper>
                {t('forms.AlarmsEditing.addAlarm')}
              </MessageWrapper>
              <ButtonWrapper>
                <Button
                  info
                  transparent
                  onPress={() => fields.push(emptyAlarm)}
                >
                  <Icon name="add"/>
                </Button>
              </ButtonWrapper>
            </AddNew>
          </Wrapper>
        )}
      </NamespacesConsumer>
    );
  }
}

function mapStateToProps(state: ReduxStateType) {
  return {
    selectItem: selectItemField(state),
  };
}

export default connect(mapStateToProps, { deleteMyMedicationAlarm })(AlarmFormPart);
