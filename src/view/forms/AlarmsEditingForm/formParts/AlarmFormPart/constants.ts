export const daysAWeek = [
  {
    value: 0,
    label: 'forms.AlarmsEditing.weekDays.monday',
  },
  {
    value: 1,
    label: 'forms.AlarmsEditing.weekDays.tuesday',
  },
  {
    value: 2,
    label: 'forms.AlarmsEditing.weekDays.wednesday',
  },
  {
    value: 3,
    label: 'forms.AlarmsEditing.weekDays.thursday',
  },
  {
    value: 4,
    label: 'forms.AlarmsEditing.weekDays.friday',
  },
  {
    value: 5,
    label: 'forms.AlarmsEditing.weekDays.saturday',
  },
  {
    value: 6,
    label: 'forms.AlarmsEditing.weekDays.sunday',
  },
];
