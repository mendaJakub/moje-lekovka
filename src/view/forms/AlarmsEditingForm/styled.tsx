import styled from 'styled-components/native';
import { Icon, Text } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

export const Wrapper = styled.View`
  display: flex;
  flex: 1 1 auto;
  align-items: stretch;
  flex-direction: column;
  margin-bottom: 20px;
`;

export const FieldsWrapper = styled.View`
  flex: 1;
`;

export const ButtonWrapper = styled.View`
  flex: 0 1 auto;
  align-self: flex-end;
`;

export const SubmitButtonWrapper = styled.View`
  flex: 1;
`;

export const MessageWrapper = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;

export const AlarmContent = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
  margin-bottom: 20px;
  display: flex;
  flex-direction: row;
  flex: 1;
`;

export const AlarmContentSection = styled.View`
  flex: 1;
  flex-basis: 50%;
`;

export const Label = styled(Text)`
  font-weight: bold;
  margin-bottom: 20px;
`;

export const AddNew = styled.TouchableOpacity`
  padding: 10px 0 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const AlarmHeading = styled(Text)`
  font-size: 20px;
  color: ${(props: StyledWithTheme) => props.theme.colors.primary};
  font-weight: bold;
`;

export const AlarmWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const DeleteButtonWrapper = styled.View`
`;

export const HeadingIcon = styled(Icon)`
  color: ${(props: StyledWithTheme) => props.theme.colors.primary};
  margin-right: 20px;
`;

export const Header = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 20px;
`;
