import React, { Component } from 'react';
import { Form, Button, Text } from 'native-base';
import { FieldArray, InjectedFormProps, reduxForm } from 'redux-form';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { SubmitButtonWrapper } from './styled';
import { Alarm } from '../../../lib/ApiService/medication/types';
import { editMyAlarms, getMyMedicationDetail } from '../../../state/modules/my/actions';
import AlarmFormPart from './formParts/AlarmFormPart';
import { connect } from 'react-redux';
import { ReduxStateType } from '../../../state/rootReducer';
import { selectMyMedicationDetailRoot } from '../../../state/modules/my/selectors';
import { MedicationTakingItem } from '../../../state/modules/my/@types';

export const FORM_NAME = 'AlarmsEditingForm';

export interface FormData {
  alarms: Alarm[];
  medicationId: string;
}

interface ComponentProps {
  medicationTakingId: string;
}

interface StateProps {
  medicationDetail: MedicationTakingItem;
}

interface DispatchProps {
  getMyMedicationDetail: typeof getMyMedicationDetail;
}

interface Props extends InjectedFormProps<FormData>, ComponentProps, WithNamespaces, StateProps, DispatchProps {

}

class AlarmsEditingForm extends Component<Props> {
  componentDidMount() {
    const { medicationTakingId, medicationDetail } = this.props;

    if (!medicationDetail) {
      this.props.getMyMedicationDetail(medicationTakingId);
    }
  }

  render() {
    const { handleSubmit, submitting, medicationTakingId, t } = this.props;

    return (
      <>
        <Form>
          <FieldArray
            name="alarms"
            component={AlarmFormPart}
            medicationId={medicationTakingId}
          />
        </Form>
        <SubmitButtonWrapper>
          <Button
            full
            primary={!submitting}
            light={submitting}
            disabled={submitting}
            bordered={submitting}
            onPress={handleSubmit(editMyAlarms)}
          >
            <Text>{t('forms.AlarmsEditing.submit')}</Text>
          </Button>
        </SubmitButtonWrapper>
      </>
    );
  }
}

function mapStateToProps(state: ReduxStateType, props: ComponentProps) {
  const medicationDetail = selectMyMedicationDetailRoot(props.medicationTakingId)(state);

  return {
    medication: medicationDetail,
    initialValues: medicationDetail ? {
      alarms: medicationDetail.alarms,
      medicationId: medicationDetail._id,
    } : null,
  };
}

const WithReduxForm = reduxForm({
  form: FORM_NAME,
})(AlarmsEditingForm);

const Connected = connect(mapStateToProps, { getMyMedicationDetail })(WithReduxForm);

export default withNamespaces()(Connected as any) as any;
