import React, { Component } from 'react';
import { Form, Button, Text } from 'native-base';
import TextInput from '../../components/redux-form-bindings/TextInput';
import { InjectedFormProps, reduxForm } from 'redux-form';
import { resetPassword } from '../../../state/modules/auth/actions';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import validate from './validation';
import { ButtonWrapper } from './styled';

export const FORM_NAME = 'ResetPasswordForm';

export interface FormData {
  email: string;
}

interface ComponentProps {
}

interface Props extends InjectedFormProps<FormData>, ComponentProps, WithNamespaces {

}

class ResetPasswordForm extends Component<Props> {
  render() {
    const { handleSubmit, submitting, t } = this.props;

    return (
      <>
        <Form>
          <TextInput
            label={t('forms.ResetPasswordForm.email')}
            name="email"
            editable={!submitting}
          />
        </Form>
        <ButtonWrapper>
          <Button
            full
            primary={!submitting}
            light={submitting}
            disabled={submitting}
            bordered={submitting}
            onPress={handleSubmit(resetPassword)}
          >
            <Text>{t('forms.ResetPasswordForm.submit')}</Text>
          </Button>
        </ButtonWrapper>
      </>
    );
  }
}

const WithReduxForm = reduxForm({
  form: FORM_NAME,
  validate,
})(ResetPasswordForm);

export default withNamespaces()(WithReduxForm as any) as any;
