import React, { Component } from 'react';
import { Form, Button, Text } from 'native-base';
import TextInput from '../../components/redux-form-bindings/TextInput';
import { InjectedFormProps, reduxForm } from 'redux-form';
import { signUp } from '../../../state/modules/auth/actions';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import validate from './validation';
import { ButtonWrapper } from './styled';
import { TextInput as NativeInput } from 'react-native';
import Modal from '../../components/SubmissionModal';

export const FORM_NAME = 'SignUpForm';

export interface FormData {
  email: string;
  password: string;
  passwordMatch: string;
  firstName: string;
  lastName: string;
}

interface ComponentProps {
  registerRef?: (c: SignUpForm) => void;
}

interface Props extends InjectedFormProps<FormData>, ComponentProps, WithNamespaces {
  validate: () => any;
}

interface State {
  modalOpen: boolean;
}

class SignUpForm extends Component<Props, State> {
  private inputs: NativeInput[] = [];
  private ahoj: any;

  constructor(props: Props) {
    super(props);

    this.state = {
      modalOpen: false,
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount() {
    if (this.props.registerRef) {
      this.props.registerRef(this);
    }
  }

  private async openModal() {
    const { valid } = this.props;
    this.props.validate();
    this.props.touch('firstName', 'lastName', 'email', 'password', 'passwordMatch');

    if (valid) {
    this.setState({ modalOpen: true });
    }
  }

  public closeModal() {
    this.setState({ modalOpen: false });
  }

  render() {
    const { handleSubmit, submitting, t } = this.props;

    return (
      <>
        <Form>
          <TextInput
            autoFocus
            label={t('forms.SignUpForm.firstName')}
            name="firstName"
            returnKeyType="next"
            editable={!submitting}
          />
          <TextInput
            label={t('forms.SignUpForm.lastName')}
            name="lastName"
            editable={!submitting}
          />
          <TextInput
            keyboardType="email-address"
            label={t('forms.SignUpForm.email')}
            name="email"
            autoCorrect={false}
            editable={!submitting}
          />
          <TextInput
            label={t('forms.SignUpForm.password')}
            name="password"
            secureTextEntry
            editable={!submitting}
          />
          <TextInput
            label={t('forms.SignUpForm.passwordMatch')}
            name="passwordMatch"
            secureTextEntry
            lastItem
            onSubmitEditing={this.openModal}
            returnKeyType="go"
            editable={!submitting}
          />
        </Form>
        <ButtonWrapper>
          <Button
            disabled={submitting}
            full
            primary={!submitting}
            light={submitting}
            bordered={submitting}
            onPress={this.openModal}
          >
            <Text>{t('forms.SignUpForm.submit')}</Text>
          </Button>
        </ButtonWrapper>
        <Modal
          modalOpen={this.state.modalOpen}
          onClose={this.closeModal}
          onSubmit={handleSubmit(signUp)}
          submitting={submitting}
          infoText={t('forms.SignUpForm.submitQuestion')}
          submitText={t('forms.SignUpForm.submit')}
          cancelText={t('forms.SignUpForm.cancel')}
        />
      </>
    );
  }
}

const WithReduxForm = reduxForm({
  form: FORM_NAME,
  validate,
})(SignUpForm);

export default withNamespaces()(WithReduxForm as any) as any;
