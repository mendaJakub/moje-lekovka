import { createValidator, required, email, minLength, maxLength, equalPasswords } from '../../../utils/validation';

export default createValidator({
  email: [required, email],
  password: [required, minLength(5)],
  passwordMatch: [required, equalPasswords('password')],
  firstName: [required, maxLength(50)],
  lastName: [required, maxLength(50)],
});
