import { createValidator, required, maxLength } from '../../../utils/validation';

export default createValidator({
  title: [required, maxLength(80)],
  medicationId: [required],
});
