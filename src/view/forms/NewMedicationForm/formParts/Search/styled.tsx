import styled from 'styled-components/native';
import { List, Input, Icon, Text } from 'native-base';
import { StyledWithTheme } from '../../../../theme/DefaultTheme';
import { PrescriptionCode } from '../../../../../lib/ApiService/medication/types';
import { PrescriptionCodes } from '../../../../../constants';

interface StyledInputProps extends StyledWithTheme {
  isInactive: boolean;
}

interface StyledSearchIconProps extends StyledWithTheme {
  isInactive: boolean;
}

interface ListItemIconProps extends StyledWithTheme {
  prescription: PrescriptionCode;
}

export const Results = styled(List)`
`;

export const StyledInput = styled(Input)`
  ${(props: StyledInputProps) => {
    if (props.isInactive) {
      return `
        border-color: ${props.theme.colors.lighterGrey};
        color: ${props.theme.colors.lighterGrey};
      `;
    }
  }}
`;

export const StyledSearchIcon = styled(Icon)`
  ${(props: StyledSearchIconProps) => {
  if (props.isInactive) {
    return `
        color: ${props.theme.colors.lighterGrey};
      `;
  }
}}
`;

export const NoContentWrapper = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 40px;
  width: 50%;
  align-self: center;
`;

export const NoContentIcon = styled(Icon)`
  font-size: 50px;
  margin-bottom: 10px;
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;

export const NoContentText = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
  text-align: center;
`;

export const ListItemIcon = styled(Icon)`
  color: ${(props: ListItemIconProps) => resolveListItemIconColor(props.prescription, props.theme)};
  margin-right: 10px;
`;

export const ListItemContent = styled.View`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const ListItemName = styled.Text`
  margin-bottom: 5px;
`;

export const ListItemDescription = styled.Text`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
`;

const resolveListItemIconColor = (prescriptionType: PrescriptionCode, theme: StyledWithTheme['theme']) => {
  switch(prescriptionType) {
    case PrescriptionCodes.LIMITED:
      return theme.colors.danger;
    case PrescriptionCodes.NO_PRESCRIPTION_LIMITED_RLPO:
    case PrescriptionCodes.NO_PRESCRIPTION_LIMITED:
    case PrescriptionCodes.PRESCRIPTION_LIMITED_2:
    case PrescriptionCodes.PRESCRIPTION_LIMITED:
    case PrescriptionCodes.PRESCRIPTION:
      return theme.colors.warning;
    case PrescriptionCodes.NO_PRESCRIPTION:
      return theme.colors.primary;
  }
};
