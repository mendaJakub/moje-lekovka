import React, { Component } from 'react';
import { Text, ListItem, Button, Item, Spinner } from 'native-base';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { MedicationPreview, PrescriptionCode } from '../../../../../lib/ApiService/medication/types';
import {
  Results,
  StyledInput,
  StyledSearchIcon,
  NoContentWrapper,
  NoContentIcon,
  NoContentText,
  ListItemIcon,
  ListItemContent,
  ListItemDescription,
  ListItemName,
} from './styled';
import { PrescriptionCodes } from '../../../../../constants';

interface Props extends WithNamespaces {
  searchResults: MedicationPreview[];
  loading: boolean;
  search: (searchString: string) => void;
  onSelect: (medicationId: string) => void;
  disabled?: boolean;
  fixedValue?: string;
}

interface State {
  searchString: string;
  busy: boolean;
}

const REQUEST_THROTTLE = 500;

class Search extends Component<Props, State> {
  private requestTimeout: any = null;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchString: props.fixedValue || '',
      busy: false,
    };

    this.debouncedSearch = this.debouncedSearch.bind(this);
    this.handleSearchPress = this.handleSearchPress.bind(this);
    this.renderResultItem = this.renderResultItem.bind(this);
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.searchResults !== this.props.searchResults && this.state.busy) {
      this.setState({ busy: false });
    }
  }

  private debouncedSearch(s: string) {
    const value = s || '';

    if (value.trim().length > 1) {
      if (this.requestTimeout) {
        clearTimeout(this.requestTimeout);
      }
      this.requestTimeout = setTimeout(() => {
        this.props.search(value);
        this.requestTimeout = null;
      }, REQUEST_THROTTLE);
    }

    this.setState({ searchString: s });
  }

  private handleSearchPress() {
    const { searchString } = this.state;

    this.setState({ busy: true });
    this.props.search(searchString);
  }

  private resolvePrescriptionIcon(prescriptionType: PrescriptionCode) {
    switch(prescriptionType) {
      case PrescriptionCodes.NO_PRESCRIPTION:
      case PrescriptionCodes.NO_PRESCRIPTION_LIMITED:
      case PrescriptionCodes.NO_PRESCRIPTION_LIMITED_RLPO:
        return 'checkmark-circle';
      case PrescriptionCodes.PRESCRIPTION:
      case PrescriptionCodes.PRESCRIPTION_LIMITED:
      case PrescriptionCodes.PRESCRIPTION_LIMITED_2:
        return 'clipboard';
      case PrescriptionCodes.LIMITED:
        return 'close-circle';
      default:
        return 'medkit';
    }
  }

  renderResultItem(item: MedicationPreview) {
    return (
      <ListItem button onPress={() => this.props.onSelect(item._id)}>
        {item.prescription && <ListItemIcon
          prescription={item.prescription.code}
          name={this.resolvePrescriptionIcon(item.prescription.code)}
        />}
        <ListItemContent>
          <ListItemName>{item.name}</ListItemName>
          <ListItemDescription>
            {item.nameAddition}
          </ListItemDescription>
          {item.prescription && <ListItemDescription>
            {item.prescription.name}
          </ListItemDescription>}
        </ListItemContent>
      </ListItem>
    );
  }

  renderNoContent() {
    const { t } = this.props;

    return (
      <NoContentWrapper>
        <NoContentIcon name="medkit" />
        <NoContentText>{t('forms.NewMedicationForm.noContent')}</NoContentText>
      </NoContentWrapper>
    );
  }

  render() {
    const { searchResults, disabled, loading, t } = this.props;
    const { searchString, busy } = this.state;

    return (
      <>
        <Item>
          <StyledSearchIcon
            name="ios-search"
            isInactive={disabled || busy}
          />
          <StyledInput
            isInactive={busy || disabled}
            disabled={busy || disabled}
            placeholder={t('forms.NewMedicationForm.placeholder')}
            value={searchString}
            onChangeText={this.debouncedSearch}
          />
          <Button
            primary={!disabled && !busy}
            light={disabled || busy}
            disabled={disabled || busy}
            transparent
            onPress={this.handleSearchPress}
          >
            <Text>{t('forms.NewMedicationForm.search')}</Text>
          </Button>
        </Item>
        {loading && <Spinner/>}
        {!disabled && <Results
          dataArray={searchResults || []}
          renderRow={this.renderResultItem}
        />}
        {(!searchResults && !disabled && !loading) && this.renderNoContent()}
      </>
    );
  }
}

export default withNamespaces()(Search);
