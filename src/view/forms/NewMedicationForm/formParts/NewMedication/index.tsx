import React from 'react';
import { Form } from 'native-base';
import TextInput from '../../../../components/redux-form-bindings/TextInput';
import { NamespacesConsumer } from 'react-i18next';

interface Props {
  submitting: boolean;
  onSubmit: () => void;
}

export default ({ submitting, onSubmit }: Props) => {
  return (
    <NamespacesConsumer ns={null}>
      {(t) => (
        <>
          <Form>
            <TextInput
              label={t('forms.NewMedicationForm.title')}
              name="title"
              editable={!submitting}
              onSubmitEditing={onSubmit}
              stackedLabel
              placeholder={t('forms.NewMedicationForm.title')}
            />
          </Form>
        </>
      )}
    </NamespacesConsumer>
  );
};
