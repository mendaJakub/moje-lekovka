import React, { Component } from 'react';
import { InjectedFormProps, reduxForm } from 'redux-form';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import validate from './validation';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ReduxStateType } from '../../../state/rootReducer';
import {
  selectSearchDetail, selectSearchDetailError,
  selectSearchDetailLoading, selectSearchLoading,
  selectSearchResults,
} from '../../../state/modules/search/selectors';
import { Medication, MedicationPreview } from '../../../lib/ApiService/medication/types';
import { clearSearchState, getMedicationDetail, searchForMedication } from '../../../state/modules/search/actions';
import NewMedicationFormPart from './formParts/NewMedication';
import SearchFormPart from './formParts/Search';
import { Dispatch } from 'redux';
import MedicationInfo from '../../components/MedicationInfo';
import { selectMyMedicationDetail, selectMyMedicationDetailRoot } from '../../../state/modules/my/selectors';
import {
  SearchFormWrapper,
  MedicationTitleFormWrapper,
  MedicationInfoWrapper,
  InfoHeading,
  ButtonWrapper,
  LoaderWrapper,
} from './styled';
import { Button, Spinner, Text } from 'native-base';
import { addMyMedication, editMyMedication } from '../../../state/modules/my/actions';

export const FORM_NAME = 'NewMedicationForm';

export interface FormData {
  title: string;
  medicationId: string;
  id: string;
}

interface ComponentProps {
  editingId?: string;
}

interface TranslatedProps extends ComponentProps, WithNamespaces {
}

interface StateProps {
  searchResults: MedicationPreview[];
  searchResultsLoading: boolean;
  detail: Medication;
  detailLoading: boolean;
  detailError: Error;
}

interface DispatchProps {
  search: typeof searchForMedication;
  getMedicationDetail: typeof getMedicationDetail;
  clearSearchState: typeof clearSearchState;
}

interface ConnectedProps extends TranslatedProps, StateProps, DispatchProps {
}

interface Props extends InjectedFormProps<FormData, ConnectedProps>, ConnectedProps {
}

class NewMedicationForm extends Component<Props> {
  constructor(props: Props) {
    super(props);

    this.handleSelectSearchOption = this.handleSelectSearchOption.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillUnmount() {
    this.props.clearSearchState();
  }

  private handleSelectSearchOption(medicationId: string) {
    this.props.getMedicationDetail(medicationId);
  }

  private handleSubmit(data: FormData, ...varArgs: any) {
    const { editingId } = this.props;

    if (editingId) {
      editMyMedication(data, ...varArgs);
    } else {
      addMyMedication(data, ...varArgs);
    }
  }

  render() {
    const { search, searchResults, searchResultsLoading, detail, detailLoading, editingId, handleSubmit, submitting, t } = this.props;

    return (
      <>
        {!Boolean(editingId) && (
          <SearchFormWrapper>
            <SearchFormPart
              searchResults={searchResults}
              loading={searchResultsLoading}
              search={search}
              onSelect={this.handleSelectSearchOption}
              disabled={Boolean(detail) || detailLoading}
              fixedValue={detail ? detail.name : undefined}
            />
          </SearchFormWrapper>
        )}
        {detailLoading && (
          <LoaderWrapper>
            <Spinner/>
          </LoaderWrapper>
        )}
        {detail && <MedicationTitleFormWrapper>
          <NewMedicationFormPart
            submitting={submitting}
            onSubmit={handleSubmit(this.handleSubmit)}
          />
        </MedicationTitleFormWrapper>}
        {detail && <InfoHeading>{t('forms.NewMedicationForm.infoHeading')}</InfoHeading>}
        {detail && (
          <MedicationInfoWrapper>
            <MedicationInfo medication={detail}/>
          </MedicationInfoWrapper>
        )}
        {detail && (
          <ButtonWrapper>
            <Button
              full
              primary={!submitting}
              light={submitting}
              disabled={submitting}
              bordered={submitting}
              onPress={handleSubmit(this.handleSubmit)}
            >
              <Text>{editingId ? t('forms.NewMedicationForm.edit') : t('forms.NewMedicationForm.submit')}</Text>
            </Button>
          </ButtonWrapper>
        )}
      </>
    );
  }
}

const mapStateToProps: MapStateToProps<StateProps, TranslatedProps, ReduxStateType> = (state: ReduxStateType, props: TranslatedProps) => {
  let detail: Medication = null;

  if (props.editingId) {
    detail = selectMyMedicationDetail(props.editingId)(state);
  } else {
    detail = selectSearchDetail(state);
  }

  return {
    searchResults: selectSearchResults(state),
    searchResultsLoading: selectSearchLoading(state),
    detail: detail,
    detailLoading: selectSearchDetailLoading(state),
    detailError: selectSearchDetailError(state),
    initialValues: detail ? {
      title: props.editingId ? selectMyMedicationDetailRoot(props.editingId)(state).title : detail.name,
      medicationId: detail._id,
      id: props.editingId || undefined,
    } : null,
  };
};

const mapDispatchToProps: MapDispatchToProps<DispatchProps, TranslatedProps> = (dispatch: Dispatch) => {
  return {
    search: (searchString: string) => dispatch(searchForMedication(searchString)),
    getMedicationDetail: (id: string) => dispatch(getMedicationDetail(id)),
    clearSearchState: () => dispatch(clearSearchState()),
  };
};

const WithReduxForm = reduxForm({
  form: FORM_NAME,
  validate,
  enableReinitialize: true,
})(NewMedicationForm);

const Connected = connect(mapStateToProps, mapDispatchToProps)(WithReduxForm);

export default withNamespaces()(Connected);
