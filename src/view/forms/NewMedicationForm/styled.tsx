import styled from 'styled-components/native';
import { StyledWithTheme } from '../../theme/DefaultTheme';
import { Text } from 'native-base';

export const MedicationInfoWrapper = styled.View`
  margin: 10px 0;
  padding: 10px 0;
  border-bottom-width: 1px;
  border-top-width: 1px;
  border-color: ${(props: StyledWithTheme) => props.theme.colors.dirtyWhite};
`;

export const SearchFormWrapper = styled.View`
  margin-bottom: 10px;
`;

export const MedicationTitleFormWrapper = styled.View`
  margin: 20px 0 40px;
`;

export const InfoHeading = styled(Text)`
  font-size: 20px;
  font-weight: bold;
  margin-top: 10px;
  text-align: center;
`;

export const ButtonWrapper = styled.View`
  padding: 40px 0 20px;
`;

export const LoaderWrapper = styled.View`
  padding: 30px 0;
`;
