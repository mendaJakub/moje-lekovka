import React, { Component } from 'react';
import { Form, Button, Text } from 'native-base';
import TextInput from '../../components/redux-form-bindings/TextInput';
import { InjectedFormProps, reduxForm } from 'redux-form';
import { signIn } from '../../../state/modules/auth/actions';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import validate from './validation';
import { ButtonWrapper } from './styled';

export const FORM_NAME = 'SignInForm';

export interface FormData {
  email: string;
  password: string;
}

interface ComponentProps {
}

interface Props extends InjectedFormProps<FormData>, ComponentProps, WithNamespaces {

}

class SignInForm extends Component<Props> {
  render() {
    const { handleSubmit, submitting, t } = this.props;

    return (
      <>
        <Form>
          <TextInput
            label={t('forms.SignInForm.email')}
            name="email"
            editable={!submitting}
          />
          <TextInput
            label={t('forms.SignInForm.password')}
            name="password"
            secureTextEntry
            lastItem
            returnKeyType="go"
            onSubmitEditing={handleSubmit(signIn)}
            editable={!submitting}
          />
        </Form>
        <ButtonWrapper>
          <Button
            full
            primary={!submitting}
            light={submitting}
            disabled={submitting}
            bordered={submitting}
            onPress={handleSubmit(signIn)}
          >
            <Text>{t('forms.SignInForm.submit')}</Text>
          </Button>
        </ButtonWrapper>
      </>
    );
  }
}

const WithReduxForm = reduxForm({
  form: FORM_NAME,
  validate,
})(SignInForm);

export default withNamespaces()(WithReduxForm as any) as any;
