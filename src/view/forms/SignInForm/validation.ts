import { createValidator, required, email, minLength } from '../../../utils/validation';

export default createValidator({
  email: [required, email],
  password: [required, minLength(5)],
});
