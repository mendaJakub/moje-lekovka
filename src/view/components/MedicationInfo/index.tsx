import React, { ReactElement } from 'react';
import {
  Wrapper,
  MedicationName,
  Label,
  Value,
  Section,
  SubTitle,
  InfoContent,
  StyledIcon,
  StyledAccordion,
  ItemContentWrapper,
} from './styled';
import { Medication, SubstancePreview } from '../../../lib/ApiService/medication/types';
import { NamespacesConsumer } from 'react-i18next';
import { ListItem } from 'native-base';
import theme from '../../theme/DefaultTheme';

interface Props {
  medication: Medication;
}

export default ({ medication }: Props) => {
  return (
    <NamespacesConsumer ns={null}>
      {(t) => {
        const headerStyle = {
          backgroundColor: 'transparent',
          borderBottomWidth: 1,
          borderBottomColor: theme.colors.lightGrey,
        };
        const renderContent = (item: { title: string; content: ReactElement<any> }) => item.content;
        const accordionContent = [
          {
            title: t('components.MedicationInfo.basicInfo'),
            content: (
              <ItemContentWrapper>
                {Boolean(medication.prescription) && (
                  <ListItem>
                    <StyledIcon name="document" />
                    <Section>
                      <Label>{t('components.MedicationInfo.prescription')}</Label>
                      <Value>{medication.prescription.name}</Value>
                    </Section>
                  </ListItem>
                )}
                {Boolean(medication.usage) && (
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Label>{t('components.MedicationInfo.usage')}</Label>
                      <Value>{medication.usage}</Value>
                    </Section>
                  </ListItem>
                )}
                {Boolean(medication.dailyDose) && (
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Label>{t('components.MedicationInfo.dailyDose')}</Label>
                      <Value>{medication.dailyDose.value} {medication.dailyDose.unit}</Value>
                    </Section>
                  </ListItem>
                )}
                {Boolean(medication.power) && (
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Label>{t('components.MedicationInfo.power')}</Label>
                      <Value>{medication.power}</Value>
                    </Section>
                  </ListItem>
                )}
                {Boolean(medication.form) && (
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Label>{t('components.MedicationInfo.form')}</Label>
                      <Value>{medication.form}</Value>
                    </Section>
                  </ListItem>
                )}
                {(medication.expiration && medication.expiration.value) ? (
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Label>{t('components.MedicationInfo.expiration')}</Label>
                      <Value>{medication.expiration.value} {medication.expiration.unit}</Value>
                    </Section>
                  </ListItem>
                ) : null}
              </ItemContentWrapper>
            ),
          },
        ];

        if (medication.indicationGroup) {
          accordionContent.push(
            {
              title: t('components.MedicationInfo.indicationGroup'),
              content: (
                <ItemContentWrapper>
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Value>{medication.indicationGroup.name}</Value>
                    </Section>
                  </ListItem>
                </ItemContentWrapper>
              ),
            },
          );
        }

        if (medication.addictions || medication.doping) {
          accordionContent.push(
            {
              title: t('components.MedicationInfo.addictionsAndDoping'),
              content: (
                <ItemContentWrapper>
                  {Boolean(medication.addictions) && (
                    <ListItem>
                      <StyledIcon name="information-circle-outline" />
                      <Section>
                        <Label>{t('components.MedicationInfo.addictions')}</Label>
                        <Value>{medication.addictions.name}</Value>
                      </Section>
                    </ListItem>
                  )}
                  {Boolean(medication.doping) && (
                    <ListItem>
                      <StyledIcon name="information-circle-outline" />
                      <Section>
                        <Label>{t('components.MedicationInfo.doping')}</Label>
                        <Value>{medication.doping.name}</Value>
                      </Section>
                    </ListItem>
                  )}
                </ItemContentWrapper>
              ),
            },
          );
        }

        if (medication.substances) {
          accordionContent.push(
            {
              title: t('components.MedicationInfo.substances'),
              content: (
                <ItemContentWrapper>
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      {medication.substances.map((substance: SubstancePreview, sqc: number) => (<Value key={substance._id}>{substance.name}{sqc + 1 < medication.substances.length ? ', ' : ''}</Value>))}
                    </Section>
                  </ListItem>
                </ItemContentWrapper>
              ),
            },
          );
        }

        if (medication.atcWho) {
          accordionContent.push(
            {
              title: t('components.MedicationInfo.atc'),
              content: (
                <ItemContentWrapper>
                  <ListItem>
                    <StyledIcon name="information-circle-outline" />
                    <Section>
                      <Value>{medication.atcWho.name}</Value>
                    </Section>
                  </ListItem>
                </ItemContentWrapper>
              ),
            },
          );
        }

        return (
          <Wrapper>
            <MedicationName>
              {medication.name}
            </MedicationName>
            <SubTitle>
              {(medication.nameAddition || '').toLowerCase()}
            </SubTitle>
            <InfoContent>
              <StyledAccordion
                dataArray={accordionContent}
                renderContent={renderContent}
                icon="ios-arrow-down"
                expandedIcon="ios-arrow-up"
                headerStyle={headerStyle}
              />
            </InfoContent>
          </Wrapper>
        );
      }}
    </NamespacesConsumer>
  );
};
