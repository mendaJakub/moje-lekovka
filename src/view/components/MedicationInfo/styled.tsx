import styled from 'styled-components/native';
import { Text, Icon, Accordion } from 'native-base';
import { StyledWithTheme } from '../../theme/DefaultTheme';

export const Wrapper = styled.View`
`;

export const MedicationName = styled(Text)`
  font-size: 20px;
  margin-bottom: 5px;
  font-weight: bold;
  color: ${(props: StyledWithTheme) => props.theme.colors.primary};
`;

export const SubTitle = styled(Text)`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
  margin-bottom: 20px;
`;

export const InfoContent = styled.View`
`;

export const Section = styled.View`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  text-align: left;
  margin-right: 60px;
`;

export const Label = styled.Text`
  font-weight: bold;
  font-size: 16px;
  color: ${(props: StyledWithTheme) => props.theme.colors.secondary};
  text-align: left;
`;

export const Value = styled.Text`
  text-align: left;
`;

export const StyledIcon = styled(Icon)`
  color: ${(props: StyledWithTheme) => props.theme.colors.lightGrey};
  margin-right: 10px;
`;

export const StyledAccordion = styled(Accordion)`

`;

export const ItemContentWrapper = styled.View`
  padding: 20px 0;
`;
