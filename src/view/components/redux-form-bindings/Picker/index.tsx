import React from 'react';
import { Picker, Label } from 'native-base';
import { Field, WrappedFieldProps } from 'redux-form';
import { PickerProps, TextInputProps } from 'react-native';
import { Wrapper, ErrorText, StyledItem } from './styled';

// FIXME

export interface PickerOption {
  label: string;
  value: string | number;
}

interface Props extends TextInputProps, InnerComponentProps {
  name: string;
  placeholder?: string;
}

interface InnerComponentProps {
  lastItem?: boolean;
  label: string;
  stackedLabel?: boolean;
  options: PickerOption[];
}

interface FieldProps extends WrappedFieldProps, PickerProps, InnerComponentProps {}

const PickerField = ({ name, placeholder, label, lastItem, ...textInputProps }: Props) => {
  return (
    <Field
      name={name}
      component={PickerComponent}
      placeholder={placeholder}
      lastItem={lastItem}
      label={label}
      {...textInputProps}
    />
  );
};

const PickerComponent = ({ input, meta, label, lastItem, stackedLabel, options, ...rest }: FieldProps) => {
  const invalid = meta.invalid && meta.touched;

  return (
    <>
      <StyledItem stackedLabel={stackedLabel} last={lastItem || false}>
        <Label>{label}</Label>
        <Wrapper>
          <Picker
            onValueChange={(text: string | number) => input.onChange(text)}
            selectedValue={input.value}
            {...rest}
          >
            {options.map((option: PickerOption) => (
              <Picker.Item label={option.label} value={option.value} />
            ))}
            <Picker.Item label="Wallet" value="key0" />
            <Picker.Item label="ATM Card" value="key1" />
          </Picker>
        </Wrapper>
      </StyledItem>
      {invalid && <ErrorText>{meta.error}</ErrorText>}
    </>
  );
};

export default PickerField;
