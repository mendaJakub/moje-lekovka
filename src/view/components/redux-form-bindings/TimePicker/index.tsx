import React, { Component } from 'react';
import { Field, WrappedFieldProps } from 'redux-form';
import { PickerProps, TextInputProps } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Wrapper, ErrorText, StyledLabel, StyledTouchableOpacity, StyledIcon, StyledPickerValue } from './styled';
import moment from 'moment';
import { WithNamespaces, withNamespaces } from 'react-i18next';

interface Props extends TextInputProps, InnerComponentProps {
  name: string;
  placeholder?: string;
  validate: (value: any) => any;
}

interface InnerComponentProps {
  label: string;
  initialValue?: string;
  error?: string;
}

interface FieldProps extends WrappedFieldProps, PickerProps, InnerComponentProps, WithNamespaces {
}

interface State {
  isVisible: boolean;
}

const TimePickerField = ({ name, placeholder, label, ...textInputProps }: Props) => {
  return (
    <Field
      name={name}
      component={withNamespaces()(TimePickerComponent)}
      placeholder={placeholder}
      label={label}
      {...textInputProps}
    />
  );
};

class TimePickerComponent extends Component<FieldProps, State> {
  constructor(props: FieldProps) {
    super(props);

    this.state = {
      isVisible: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.showPicker = this.showPicker.bind(this);
    this.hidePicker = this.hidePicker.bind(this);
  }

  private showPicker() {
    this.setState({ isVisible: true });
  }

  private hidePicker() {
    this.setState({ isVisible: false });
  }

  private handleChange(val: any) {
    const { input } = this.props;
    const valueMoment = moment(val);
    const dayMidnight = valueMoment.clone().startOf('day');
    const minutesFromMidnight = valueMoment.diff(dayMidnight, 'minutes');

    this.hidePicker();
    input.onChange([minutesFromMidnight]);
  }

  render() {
    const { input, meta, label, initialValue, error, t } = this.props;
    const { isVisible } = this.state;
    const invalid = meta.invalid && meta.touched;

    return (
      <>
        <Wrapper>
          <StyledLabel>{label}</StyledLabel>
          <StyledTouchableOpacity onPress={this.showPicker}>
            <StyledIcon name="time" hasValue={Boolean(input.value && input.value.length)} />
            <StyledPickerValue hasValue={Boolean(input.value && input.value.length)}>{(input.value && input.value.length) ? moment().startOf('day').add(input.value[0], 'minutes').format('HH:mm') : t('components.redux-form-bindings.TimePicker.empty')}</StyledPickerValue>
          </StyledTouchableOpacity>
          <DateTimePicker
            mode="time"
            date={(input.value && input.value.length)
              ? moment().startOf('day').add(input.value[0], 'minutes').toDate()
              : moment().toDate()}
            isVisible={isVisible}
            onConfirm={this.handleChange}
            onCancel={this.hidePicker}
            is24Hour
          />
        </Wrapper>
        {(invalid || error) && <ErrorText>{meta.error || error}</ErrorText>}
      </>
    );
  }
}

export default TimePickerField;
