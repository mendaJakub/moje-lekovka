import styled from 'styled-components/native';
import { Label, Text, Icon } from 'native-base';
import { StyledWithTheme } from '../../../theme/DefaultTheme';

interface StyledPickerValueProps extends StyledWithTheme {
  hasValue: boolean;
}

interface StyledIconValueProps extends StyledWithTheme {
  hasValue: boolean;
}

export const ErrorText = styled.Text`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
  align-self: flex-end;
  padding: 10px 0;
`;

export const Wrapper = styled.View`
  flex: 1;
`;

export const StyledLabel = styled(Label)`
  margin-bottom: 10px;
  font-weight: bold;
`;

export const StyledTouchableOpacity = styled.TouchableOpacity`
  color: ${(props: StyledWithTheme) => props.theme.colors.tertiary};
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 10px 0 20px;
`;

export const StyledPickerValue = styled(Text)`
  color: ${(props: StyledPickerValueProps) => props.hasValue ? props.theme.colors.tertiary : props.theme.colors.lightGrey};
`;

export const StyledIcon = styled(Icon)`
  margin-right: 20px;
  color: ${(props: StyledIconValueProps) => props.hasValue ? props.theme.colors.tertiary : props.theme.colors.lightGrey};
`;
