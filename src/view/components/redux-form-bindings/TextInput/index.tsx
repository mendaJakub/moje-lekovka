import React from 'react';
import { Input, Label } from 'native-base';
import { Field, WrappedFieldProps } from 'redux-form';
import { TextInputProps } from 'react-native';
import { Wrapper, ErrorText, StyledItem } from './styled';

interface Props extends TextInputProps, InnerComponentProps {
  name: string;
  placeholder?: string;
}

interface InnerComponentProps {
  lastItem?: boolean;
  label: string;
  floatingLabel?: boolean;
  stackedLabel?: boolean;
}

interface FieldProps extends WrappedFieldProps, TextInputProps, InnerComponentProps {}

export default ({ name, placeholder, label, lastItem, ...textInputProps }: Props) => {
  return (
    <Field
      name={name}
      component={InputComponent}
      placeholder={placeholder}
      lastItem={lastItem}
      label={label}
      {...textInputProps}
    />
  );
};

const InputComponent = ({ input, meta, label, lastItem, floatingLabel, stackedLabel, ...rest }: FieldProps) => {
  const invalid = meta.invalid && meta.touched;

  return (
    <>
      <StyledItem floatingLabel={floatingLabel} stackedLabel={stackedLabel} last={lastItem || false}>
        <Label>{label}</Label>
        <Wrapper>
          <Input
            onChangeText={(text: string) => input.onChange(text)}
            onBlur={(ev: any) => input.onBlur(ev)}
            value={input.value}
            {...rest}
          />
        </Wrapper>
      </StyledItem>
      {invalid && <ErrorText>{meta.error}</ErrorText>}
    </>
  );
};
