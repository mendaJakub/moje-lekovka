import styled from 'styled-components/native';
import { Item } from 'native-base';
import { StyledWithTheme } from '../../../theme/DefaultTheme';

export const ErrorText = styled.Text`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
  align-self: flex-end;
  padding: 10px 0;
`;

export const Wrapper = styled.View`
  flex: 1;
`;

export const StyledItem = styled(Item)`
  ${
    (props: any) => {
      if (props.floatingLabel || props.inlineLabel) {
        return 'padding: 10px 0;';
      }
    }
  }
`;
