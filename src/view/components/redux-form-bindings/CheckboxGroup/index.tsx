import React, { Component } from 'react';
import { Field, WrappedFieldProps } from 'redux-form';
import { PickerProps, TextInputProps } from 'react-native';
import { NamespacesConsumer } from 'react-i18next';
import {
  Wrapper,
  ErrorText,
  CheckBoxWrapper,
  AllCheckBoxWrapper,
  CheckBoxLabel,
  StyledCheckBox,
  StyledLabel,
  StyledTouchableOpacity,
  StyledIcon,
  OpenModalLink,
} from './styled';
import BasicModal from '../../../components/BasicModal';

interface CheckboxOption {
  value: number | string;
  label: string;
}

interface Props extends TextInputProps, InnerComponentProps {
  name: string;
  placeholder?: string;
  validate: (value: any) => any;
}

interface InnerComponentProps {
  label: string;
  options: CheckboxOption[];
  error?: string;
  isOpen: boolean;
  setOpen: () => void;
  onClose: () => void;
  openModalText: string;
  dismissModalText: string;
}

interface FieldProps extends WrappedFieldProps, PickerProps, InnerComponentProps {
}

const PickerField = ({ name, placeholder, label, ...textInputProps }: Props) => {
  return (
    <Field
      name={name}
      component={PickerComponent}
      placeholder={placeholder}
      label={label}
      {...textInputProps}
    />
  );
};

class PickerComponent extends Component<FieldProps> {
  constructor(props: FieldProps) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSelectAll = this.handleSelectAll.bind(this);
  }

  private handleChange(value: number | string) {
    const { input } = this.props;

    const checked = (input.value || []).includes(value);

    if (!checked) {
      input.onChange([...(input.value || []), value]);
    } else {
      input.onChange((input.value || []).filter((val: string | number) => val !== value));
    }
  }

  private handleSelectAll() {
    const { options, input } = this.props;
    input.onChange(options.map((option: CheckboxOption) => option.value));
  }

  private renderValues() {
    const { input, options } = this.props;

    return input.value.map((val: number | string, key: number) => {
      const op = options.find((option: CheckboxOption) => option.value === val);

      return `${op.label.substr(0, 2)}${key + 1 < input.value.length ? ', ' : ''}`;
    });
  }

  render() {
    const { input, meta, label, options, error, isOpen, setOpen, onClose, openModalText, dismissModalText } = this.props;
    const invalid = meta.invalid && meta.touched;

    return (
      <NamespacesConsumer ns={null}>
        {(t) => (
          <>
            <StyledLabel>{label}</StyledLabel>
            <StyledTouchableOpacity onPress={setOpen}>
              <StyledIcon name="calendar" hasValue={Boolean((input.value || []).length)} />
              <OpenModalLink hasValue={Boolean((input.value || []).length)}>{(input.value || []).length ? this.renderValues() : openModalText}</OpenModalLink>
            </StyledTouchableOpacity>
            <BasicModal
              modalOpen={isOpen}
              onClose={onClose}
              okText={dismissModalText}
            >
              <>
                <Wrapper>
                  <StyledLabel>{label}</StyledLabel>
                  <AllCheckBoxWrapper>
                    <StyledCheckBox
                      checked={(input.value || []).length === options.length}
                      onPress={this.handleSelectAll}
                    />
                    <CheckBoxLabel>{t('components.redux-form-bindings.CheckboxGroup.checkAll')}</CheckBoxLabel>
                  </AllCheckBoxWrapper>
                  {options.map((option: CheckboxOption) => (
                    <CheckBoxWrapper key={option.value}>
                      <StyledCheckBox
                        checked={(input.value || []).includes(option.value)}
                        onPress={() => this.handleChange(option.value)}
                      />
                      <CheckBoxLabel>{option.label}</CheckBoxLabel>
                    </CheckBoxWrapper>
                  ))}
                </Wrapper>
              </>
            </BasicModal>
            {(invalid || error) && <ErrorText>{meta.error || error}</ErrorText>}
          </>
        )}
      </NamespacesConsumer>
    );
  }
}

export default PickerField;
