import styled from 'styled-components/native';
import { Label, CheckBox, Text, Icon } from 'native-base';
import { StyledWithTheme } from '../../../theme/DefaultTheme';

interface StyledOpenModalLinkProps extends StyledWithTheme {
  hasValue: boolean;
}

interface StyledIconValueProps extends StyledWithTheme {
  hasValue: boolean;
}

export const ErrorText = styled.Text`
  color: ${(props: StyledWithTheme) => props.theme.colors.danger};
  align-self: flex-end;
  padding: 10px 0;
`;

export const Wrapper = styled.View`
  flex: 1;
`;

export const CheckBoxWrapper = styled.View`
  display: flex;
  flex-direction: row;
  flex: 1;
  margin-bottom: 10px;
`;

export const AllCheckBoxWrapper = styled(CheckBoxWrapper)`
  margin-bottom: 20px;
`;

export const StyledCheckBox = styled(CheckBox)`
  display: flex;
  margin-right: 10px;
`;

export const CheckBoxLabel = styled(Text)`
  display: flex;
  margin-left: 10px;
`;

export const StyledLabel = styled(Label)`
  margin-bottom: 10px;
  font-weight: bold;
`;

export const StyledTouchableOpacity = styled.TouchableOpacity`
  color: ${(props: StyledWithTheme) => props.theme.colors.tertiary};
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 10px 10px 20px 0;
`;

export const StyledIcon = styled(Icon)`
  color: ${(props: StyledIconValueProps) => props.hasValue ? props.theme.colors.tertiary : props.theme.colors.lightGrey};
`;

export const OpenModalLink = styled(Text)`
  margin-left: 20px;
  color: ${(props: StyledOpenModalLinkProps) => props.hasValue ? props.theme.colors.tertiary : props.theme.colors.lightGrey};
  padding-right: 20px;
`;
