import styled from 'styled-components/native';
import { Button, Text } from 'native-base';
import { Platform } from 'react-native';

export const Overlay = styled.View`
  background-color: rgba(0,0,0,0.4);
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const ModalContent = styled.View`
  background: #fff;
  width: 80%;
  padding: 5px 20px 20px;
`;

export const ModalText = styled(Text)`
  margin-bottom: 40px;
`;

export const CloseButton = styled(Button)`
  align-self: flex-end;
`;

export const Buttons = styled.View`
  display: flex;
  margin: 0 auto;
  flex-direction: ${Platform.OS === 'ios' ? 'row-reverse' : 'row'};
`;

export const OffsetButton = styled(Button)`
  margin: 0 2px;
`;
