import React from 'react';
import { Modal } from 'react-native';
import { Icon, Text } from 'native-base';
import { Overlay, ModalContent, ModalText, CloseButton, Buttons, OffsetButton } from './styled';

interface Props {
  modalOpen: boolean;
  onClose: () => void;
  onSubmit: () => void;
  submitting?: boolean;
  infoText: string;
  submitText: string;
  cancelText: string;
}

export default ({ modalOpen, onClose, onSubmit, submitting, infoText, submitText, cancelText }: Props) => {
  return (
    <Modal
      visible={modalOpen}
      onRequestClose={onClose}
      transparent
    >
      <Overlay>
        <ModalContent>
          <CloseButton
            onPress={onClose}
            iconLeft
            transparent
            dark
          >
            <Icon name="close" />
          </CloseButton>
          <ModalText>{infoText}</ModalText>
          <Buttons>
            <OffsetButton
              primary={!submitting}
              light={submitting}
              disabled={submitting}
              bordered={submitting}
              onPress={onSubmit}
            >
              <Text>{submitText}</Text>
            </OffsetButton>
            <OffsetButton
              dark
              bordered
              onPress={onClose}
            >
              <Text>{cancelText}</Text>
            </OffsetButton>
          </Buttons>
        </ModalContent>
      </Overlay>
    </Modal>
  );
};
