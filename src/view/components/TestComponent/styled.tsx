import styled from 'styled-components/native';
import { StyledWithTheme } from '../../theme/DefaultTemplate/index';

export const StyledView = styled.View`
  background: ${(props: StyledWithTheme) => props.theme.colors.secondary};
  padding: 10px 5px;
`;

export const StyledText = styled.Text`
  color: ${(props: StyledWithTheme) => props.theme.colors.primary};
  font-size: 20px;
`;
