import React from 'react';
import { StyledView, StyledText } from './styled';
import { NamespacesConsumer } from 'react-i18next';

interface Props {
}

export default (props: Props) => {
  return (
    <NamespacesConsumer ns={null}>
      {(t: any) => (
        <StyledView>
          <StyledText>{t('test')}</StyledText>
        </StyledView>
      )}
    </NamespacesConsumer>
  );
};
