import styled from 'styled-components/native';
import { StyledWithTheme } from '../../theme/DefaultTheme/index';

export const StyledView = styled.View`
  background-color: ${(props: StyledWithTheme) => props.theme.colors.primary};
  flex: 1;
  justify-content: center;
  align-items: center;
`;
