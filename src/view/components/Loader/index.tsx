import React from 'react';
import { StyledView } from './styled';
import { Spinner } from 'native-base';

export default () => {
  return (
    <StyledView>
      <Spinner color="#fff" />
    </StyledView>
  );
};
