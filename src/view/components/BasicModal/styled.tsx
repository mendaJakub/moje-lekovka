import styled from 'styled-components/native';
import { Button } from 'native-base';

export const Overlay = styled.View`
  background-color: rgba(0,0,0,0.4);
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const ModalContent = styled.View`
  background: #fff;
  width: 80%;
  padding: 5px 20px 20px;
  display: flex;
`;

export const ChildrenWrapper = styled.View`
  margin-bottom: 40px;
  flex: 1 0 auto;
  min-height: 300px;
`;

export const CloseButton = styled(Button)`
  align-self: flex-end;
`;

export const Buttons = styled.View`
  display: flex;
  margin: 0 auto;
  flex-direction: row;
  justify-content: flex-end;
`;

export const OffsetButton = styled(Button)`
  margin: 0 2px;
`;
