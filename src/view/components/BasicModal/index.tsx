import React, { ReactChild } from 'react';
import { Modal } from 'react-native';
import { Icon, Text } from 'native-base';
import { Overlay, ModalContent, ChildrenWrapper, CloseButton, Buttons, OffsetButton } from './styled';

interface Props {
  modalOpen: boolean;
  onClose: () => void;
  children: ReactChild;
  okText: string;
}

const BasicModal = ({ modalOpen, onClose, children, okText }: Props) => {
  return (
    <Modal
      visible={modalOpen}
      onRequestClose={onClose}
      transparent
    >
      <Overlay>
        <ModalContent>
          <CloseButton
            onPress={onClose}
            iconLeft
            transparent
            dark
          >
            <Icon name="close" />
          </CloseButton>
          <ChildrenWrapper>
            {children}
          </ChildrenWrapper>
          <Buttons>
            <OffsetButton
              dark
              bordered
              onPress={onClose}
            >
              <Text>{okText}</Text>
            </OffsetButton>
          </Buttons>
        </ModalContent>
      </Overlay>
    </Modal>
  );
};

export default BasicModal;
