import api from '../request';
import { SignInResponse } from '../users/types';
import {
  GetMyMedicationDetailResponse,
  GetMyMedicationResponse,
  PostMyMedicationRequestData,
  PostMyMedicationResponse,
  PutMyMedicationRequestData,
  PutMyMedicationResponse,
  DeleteMyMedicationResponse, PutMyMedicationAlarmsRequestData,
} from './types';

export default class MyApiService {
  public static async getMyNewToken(authorization: string): Promise<SignInResponse> {
    return await api('/my/new-token', {
      method: 'GET',
      headers: { authorization },
    });
  }

  public static async getMyMedication(authorization: string): Promise<GetMyMedicationResponse> {
    return await api('/my/medication', {
      method: 'GET',
      headers: { authorization },
    });
  }

  public static async getMyMedicationDetail(authorization: string, id: string): Promise<GetMyMedicationDetailResponse> {
    return await api(`/my/medication/${id}`, {
      method: 'GET',
      headers: { authorization },
    });
  }

  public static async postMyMedication(authorization: string, data: PostMyMedicationRequestData): Promise<PostMyMedicationResponse> {
    return await api('/my/medication', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { authorization },
    });
  }

  public static async putMyMedication(authorization: string, id: string, data: PutMyMedicationRequestData): Promise<PutMyMedicationResponse> {
    return await api(`/my/medication/${id}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: { authorization },
    });
  }

  public static async deleteMyMedication(authorization: string, id: string): Promise<DeleteMyMedicationResponse> {
    return await api(`/my/medication/${id}`, {
      method: 'DELETE',
      headers: { authorization },
    });
  }

  public static async putMyMedicationAlarms(authorization: string, id: string, data: PutMyMedicationAlarmsRequestData): Promise<PutMyMedicationResponse> {
    return await api(`/my/medication/${id}/alarms`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: { authorization },
    });
  }

  public static async deleteMyMedicationAlarm(authorization: string, medicationId: string, alarmId: string): Promise<{}> {
    return await api(`/my/medication/${medicationId}/alarms/${alarmId}`, {
      method: 'DELETE',
      headers: { authorization },
    });
  }
}
