import { Alarm, MedicationTakingDetail, MedicationTakingPreview } from '../medication/types';

export type GetMyMedicationResponse = MedicationTakingPreview[];

export type GetMyMedicationDetailResponse = MedicationTakingDetail;

export interface PostMyMedicationRequestData {
  title: string;
  medicationId: string;
}

export interface PutMyMedicationRequestData extends PostMyMedicationRequestData {}

export interface PostMyMedicationResponse extends MedicationTakingPreview {}

export interface PutMyMedicationResponse extends MedicationTakingPreview {}

export interface DeleteMyMedicationResponse extends MedicationTakingPreview {}

export interface PutMyMedicationAlarmsRequestData {
  alarms: Alarm[];
}
