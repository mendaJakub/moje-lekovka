import { ConfType } from './types';
import { getEnv } from '../../../config/environment';

export enum ResponseType {
  JSON,
  BLOB,
}

const Conf: ConfType = getEnv();
const API_URL: string = Conf.SERVICE_URL.api;

export class FetchError extends Error {
  status: number;
  statusText: string;
  ok: boolean;

  constructor(response: any) {
    super(response.statusText);

    this.status = response.status;
    this.statusText = response.statusText;
    this.ok = response.ok;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, FetchError.prototype);
  }
}

type ApiVersion = 'v1';

interface ApiOptions {
  apiVersion?: ApiVersion;

  [index: string]: any;
}

interface PostFetchConfig {
  allowedCodes?: number[];
  responseType?: ResponseType;
  noContentAsNull?: boolean;
}

function parse(responseType: ResponseType = ResponseType.JSON, noContentAsNull: boolean = false) {
  return (response: any) => {
    if (noContentAsNull && response.status === 204) {
      return null;
    } else {
      switch (responseType) {
        case ResponseType.BLOB:
          return response.blob().catch(() => new Blob());
        default:
          return response.json().catch(() => ({}));
      }
    }
  };
}

function checkStatus(allowedCodes?: number[]) {
  return (response: any) => {
    if (allowedCodes) {
      if (allowedCodes.indexOf(response.status) >= 0) {
        return response;
      }
    } else if (response.status >= 200 && response.status < 300) {
      return response;
    }

    throw new FetchError(response);
  };
}

function request(url, options, responseConfig: PostFetchConfig = {}) {
  return fetch(url, options)
    .then(checkStatus(responseConfig.allowedCodes))
    .then(parse(responseConfig.responseType, Boolean(responseConfig.noContentAsNull)));
}

function getParams(params?: any) {
  if (!params) {
    return '';
  }

  const paramsArray: string[] = [];
  Object.keys(params).map((key: string) => {
    if (params[key] || params[key] === 0 || params[key] === false) {
      if (Array.isArray(params[key]) && (isEncodedArray(params[key][0]) || isNaN(params[key][0]))) {
        params[key].map((singleParam: any) => {
          paramsArray.push(`${key}=${singleParam}`);
        });
      } else {
        paramsArray.push(`${key}=${params[key]}`);
      }
    }
  });

  return `?${paramsArray.join('&')}`;
}

export function createApiUrl(version: ApiVersion) {
  return `${API_URL}`;
}

export default function api(url: string, options: ApiOptions, params?: any, responseConfig?: PostFetchConfig) {
  let apiVersion: ApiVersion = 'v1',
    otherOptions = options;

  if (options.apiVersion) {
    ({ apiVersion, ...otherOptions } = options);
  }

  const serviceUrl = createApiUrl(apiVersion);

  return request(
    `${serviceUrl}${url}${getParams(params)}`,
    {
      ...otherOptions,
      headers: {
        ...otherOptions.headers,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
    responseConfig
  );
}

export function isEncodedArray(value: string) {
  const decodedValue = decodeURIComponent(value);
  const firstChar = decodedValue.substring(0, 1);
  const lastChar = decodedValue.slice(-1);

  return firstChar === '[' && lastChar === ']';
}
