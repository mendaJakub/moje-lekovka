export enum NotificationTypes {
  PUSH_NOTIFICATION = 'push-notification',
}

export type PrescriptionCode = 'C' | 'L' | 'F' | 'O' | 'P' | 'R' | 'V';

export interface Alarm {
  _id?: string;
  days: number[];
  timesOfDay: number[];
  notificationTypes: NotificationTypes[];
  notificationIds?: (string | number)[];
}

export interface SubstancePreview {
  _id: string;
  code: string;
  name: string;
}

export interface MedicationPreview {
  _id: string;
  suklCode: string;
  name: string;
  nameAddition: string;
  prescription: {
    code: PrescriptionCode;
    name: string;
  };
}

interface Medication extends MedicationPreview {
  power: string;
  form: string;
  packagingSize: string;
  packaging: string;
  usage: string;
  registration: {
    expiration?: string;
    unlimited?: boolean;
  };
  indicationGroup?: {
    _id: string;
    name: string;
  };
  atcWho: {
    _id: string;
    atc: string;
    name: string;
  };
  dailyDose: {
    value: number;
    unit: string;
    unitName: string;
  };
  substances: SubstancePreview[];
  addictions: {
    code: string;
    name: string;
  };
  doping: {
    code: string;
    name: string;
  };
  expiration: {
    value: string;
    unit: string;
  };
}

interface MedicationTaking {
  _id: string;
  userId: string;
  title: string;
  alarms: Alarm[];
}

interface MedicationTakingPreview extends MedicationTaking {
  medicationId: string;
}

interface MedicationTakingDetail extends MedicationTaking {
  medicationInfo: Medication;
}

export type MedicationSearchResponse = MedicationPreview[];
export interface GetMedicationDetailResponse extends Medication {}
