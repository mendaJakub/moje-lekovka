import api from '../request';
import { MedicationSearchResponse, GetMedicationDetailResponse } from './types';

export default class MedicationApiService {
  public static async searchForMedication(authorization: string, search: string): Promise<MedicationSearchResponse> {
    return await api('/medication/search', {
      method: 'GET',
      headers: { authorization },
    }, {
      search,
    });
  }

  public static async getMedicationDetail(authorization: string, id: string): Promise<GetMedicationDetailResponse> {
    return await api(`/medication/${id}`, {
      method: 'GET',
      headers: { authorization },
    });
  }
}
