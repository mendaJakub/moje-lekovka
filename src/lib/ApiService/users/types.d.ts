export interface User {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  imageUrl?: string;
}

export interface Token {
  token: string;
  expirationDate: string;
}

export interface PostUserRequestBody {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

export interface SignInRequestData {
  email: string;
  password: string;
}

export interface SignInResponse {
  token: Token;
  user: User;
}

export interface ResetPasswordRequestBody {
  email: string;
}

export interface SignUpResponse extends User {}
