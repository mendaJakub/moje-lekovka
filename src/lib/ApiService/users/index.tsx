import api from '../request';
import {
  PostUserRequestBody,
  ResetPasswordRequestBody,
  SignInRequestData,
  SignInResponse,
  SignUpResponse,
} from './types';

export default class UsersApiService {
  public static async signIn(data: SignInRequestData): Promise<SignInResponse> {
    return api('/users/sign-in', {
      method: 'POST',
      body: JSON.stringify(data),
    });
  }

  public static async postUser(data: PostUserRequestBody): Promise<SignUpResponse> {
    return api('/users', {
      method: 'POST',
      body: JSON.stringify(data),
    });
  }

  public static async resetPassword(data: ResetPasswordRequestBody): Promise<SignUpResponse> {
    return api('/users/reset-password', {
      method: 'PUT',
      body: JSON.stringify(data),
    });
  }
}
