import i18next from 'i18next';
import { Dispatch } from 'redux';
import { setLanguage } from '../../state/modules/language/actions';
import { supportedLanguages } from '../../utils/language';
import { Localization } from 'expo';
import csTranslation from '../../locale/cs/translation.json';
import enTranslation from '../../locale/en/translation.json';

const configureI18next = (dispatch: Dispatch) => {
  i18next
    .init(
      {
        resources: {
          cs: {
            translations: csTranslation,
          },
          en: {
            translations: enTranslation,
          },
        },
        lng: Localization.locale,
        fallbackLng: supportedLanguages,
        keySeparator: '.',
        interpolation: {
          escapeValue: false,
        },
        ns: ['translations'],
        defaultNS: 'translations',
        react: {
          wait: true,
        },
      },
      (err: any) => {
        if (err) {
          console.warn('i18n error: ', err);
        } else {
          dispatch(setLanguage(i18next.language, i18next, true));
        }
      }
    );

  return i18next;
};

let configuredI18next;

// Lazy init
export default (dispatch: Dispatch) => {
  if (!configuredI18next) {
    configuredI18next = configureI18next(dispatch);
  }

  return configuredI18next;
};
