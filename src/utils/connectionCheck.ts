import { Alert, NetInfo } from 'react-native';
import i18n from 'i18next';
import { FetchError } from '../lib/ApiService/request';

export function* requireNetConnection() {
  const isConnected = yield NetInfo.isConnected.fetch();

  if (!isConnected) {
    throw new FetchError({ status: 0, statusText: 'No net connection' });
  }
}

export function* checkNetConnection() {
  return yield NetInfo.isConnected.fetch();
}

export function offlineAlert() {
  Alert.alert(i18n.t('templates.DefaultTemplate.offlineForbiddenTitle'), i18n.t('templates.DefaultTemplate.offlineForbiddenText'));
}
