export const supportedLanguages = ['cs', 'en'];

export function isLangSupported(lang: string): boolean {
  if (!lang || (lang.length > 2 && lang[2] !== '-')) {
    return false;
  }

  const language = normalize(lang);
  for (const l of supportedLanguages) {
    if (language === l) {
      return true;
    }
  }

  return false;
}

export function normalize(lang: string): string {
  return (lang || '').substr(0, 2).toLowerCase();
}
