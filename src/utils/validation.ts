import i18next from 'i18next';

type ValidationRule = (value: string, data?: any) => string | undefined;

interface ValidationRuleNode {
  [key: string]: ValidationRule[] | ValidationRuleNode;
}

export function createValidator(rules: ValidationRuleNode) {
  return (data: object = {}) => {
    const errors = evalRules(rules, data, data);
    return errors;
  };
}

const evalRules = (ruleNode: ValidationRuleNode, value: object, data: object) => {
  const errors = {};
  for (const key in ruleNode) {
    const ruleSet = ruleNode[key];
    if (typeof ruleSet === 'object' && !Array.isArray(ruleSet)) {
      errors[key] = evalRules(ruleSet, value[key] || {}, data);
    } else {
      const rule = join([...ruleSet]); // concat enables both functions and arrays of functions
      const error = rule(value[key], data);
      if (error) {
        errors[key] = error;
      }
    }
  }
  return errors;
};
const join = (rules: ValidationRule[]) => {
  return (value: any, data: any) => {
    return rules.map((rule: any) => rule(value, data)).filter((error: any) => !!error)[0];
  };
};
export const isEmpty = (value: any) =>
  value === undefined || value === null || value === '' || value.length === 0 || value === {};

export function email(value: string) {
  if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return i18next.t('forms.errors.email');
  }
}

export function equalPasswords(field: string) {
  return (value: string, data: any) => {
    if (data) {
      if (value !== data[field]) {
        return i18next.t('forms.errors.passwordsMismatch');
      }
    }
    return '';
  };
}

export function required(value: any) {
  if (isEmpty(value) || (typeof value === 'string' && value.trim().length < 1)) {
    return i18next.t('forms.errors.required');
  }
}

export function arrayRequired(value: any) {
  if (!Array.isArray(value) || value.length === 0) {
    return i18next.t('forms.errors.required');
  }
}

export function minLength(minimum) {
  return (value: any) => {
    if (!isEmpty(value) && value.length < minimum) {
      return i18next.t('forms.errors.minLength', { minimum: minimum });
    }
  };
}

export function maxLength(maximum) {
  return (value: any) => {
    if (!isEmpty(value) && value.length > maximum) {
      return i18next.t('forms.errors.maxLength', { maximum: maximum });
    }
  };
}
