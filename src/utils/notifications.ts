import { Notifications, Permissions } from 'expo';
import moment from 'moment';
import i18n from 'i18next';
import { Alarm } from '../lib/ApiService/medication/types';
import { asyncForEach } from './async';
import { Platform } from 'react-native';

enum NotificationChannels {
  ALARMS = 'alarms',
}

export const setUpNotification = async (medicationName: string, alarm: Alarm): Promise<Alarm> => {
  if (alarm.notificationIds && alarm.notificationIds.length) {
    await cancelNotifications(alarm.notificationIds);
  }

  const notificationIds = [];

  await asyncForEach(alarm.days, async (day: number) => {
    const date = moment().startOf('isoWeek').add(day, 'days');
    let exactTime = date.add(alarm.timesOfDay[0], 'minutes');

    if (exactTime.isSameOrBefore(moment())) {
      exactTime = exactTime.add(1, 'week');
    }

    const sendTime = exactTime.toDate();
    const notificationTitle = i18n.t('utils.notifications.title', { medicationName });
    const notificationBody = i18n.t('utils.notifications.body', { medicationName, time: exactTime.format('HH:mm') });

    const localNotification = {
      title: notificationTitle,
      body: notificationBody,
      data: {
        title: notificationTitle,
        body: notificationBody,
      },
      android: {
        channelId: NotificationChannels.ALARMS,
      },
      ios: {
        sound: true,
      },
    };

    const schedulingOptions: any = {
      time: sendTime,
      repeat: 'week',
    };

    const notificationId = await Notifications.scheduleLocalNotificationAsync(
      localNotification,
      schedulingOptions,
    );

    notificationIds.push(notificationId);
  });

  return {
    ...alarm,
    notificationIds,
  };
};

export const cancelNotification = async (notificationId: string | number) => {
  await Notifications.cancelScheduledNotificationAsync(notificationId);
};

export const cancelNotifications = async (ids: Array<string | number>) => {
  await asyncForEach(ids, async (id: string | number) => {
    await Notifications.cancelScheduledNotificationAsync(id);
  });
};

export const getiOSNotificationPermission = async () => {
  const { status } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS,
  );

  if (status !== 'granted') {
    await Permissions.askAsync(Permissions.NOTIFICATIONS);
  }
};

export const setUpAndroidNotificationChannels = async () => {
  if (Platform.OS === 'android') {
    await Notifications.deleteChannelAndroidAsync(NotificationChannels.ALARMS);
    await Notifications.createChannelAndroidAsync(NotificationChannels.ALARMS, {
      name: 'Alarms',
      sound: true,
      vibrate: [0, 1000, 250, 1000, 250, 1000],
      priority: 'max',
    });
  }
};
