import { Toast } from 'native-base';
import { FetchError } from '../lib/ApiService/request';
import i18n from 'i18next';

export function handleError(err: FetchError, defaultError: string) {
  switch(err.status) {
    case 0:
      return Toast.show({
        text: i18n.t('flashes.noNetConnection'),
        type: 'danger',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
    case 401:
      return Toast.show({
        text: i18n.t('flashes.401'),
        type: 'danger',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
    case 403:
      return Toast.show({
        text: i18n.t('flashes.403'),
        type: 'danger',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
    case 404:
      return Toast.show({
        text: i18n.t('flashes.404'),
        type: 'danger',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
    default:
      return Toast.show({
        text: defaultError,
        type: 'danger',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
  }
}
