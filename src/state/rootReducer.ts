import { reducer as form, FormStateMap } from 'redux-form';
import auth from './modules/auth/reducer';
import my from './modules/my/reducer';
import search from './modules/search/reducer';
import { AuthState } from './modules/auth/@types';
import { MyState } from './modules/my/@types';
import { SearchState } from './modules/search/@types';

export interface ReduxStateType {
  auth: AuthState;
  form: FormStateMap;
  my: MyState;
  search: SearchState;
}

export default {
  auth,
  form,
  my,
  search,
};
