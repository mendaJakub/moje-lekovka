import { takeLatest, put, cps, fork, CpsCallback } from 'redux-saga/effects';
import { Types, languageChanged } from './actions';

import moment from 'moment';
import { normalize, isLangSupported } from '../../../utils/language';
import { AnyAction } from 'redux';

function* watchSetLanguage() {
  yield takeLatest(Types.SET_LANGUAGE, function*(action: AnyAction) {
    const { lang, i18n, forceUpdate } = action.payload;

    if (!isLangSupported(lang)) {
      return;
    }

    // Detect language change

    const selectedLanguage = normalize(i18n.language);
    const newLanguage = normalize(lang);
    if (!forceUpdate && selectedLanguage === newLanguage) {
      return;
    }

    // Init i18n with new language

    try {
      yield cps((newLang: string, cb: CpsCallback<any>) => {
        i18n.changeLanguage(newLang, cb);
      }, newLanguage);
    } catch (err) {
      console.error(`Failed to change language to '${newLanguage}'`, err);
      return;
    }

    moment.locale(newLanguage);
    yield put(languageChanged(selectedLanguage, newLanguage));
  });
}

export default function*() {
  yield fork(watchSetLanguage);
}
