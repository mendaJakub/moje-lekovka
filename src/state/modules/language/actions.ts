import i18next from 'i18next';

export enum Types {
  SET_LANGUAGE = 'language/SET',
  LANGUAGE_CHANGED = 'language/CHANGE',
}

export function setLanguage(lang: string, i18n: i18next.i18n, forceUpdate?: boolean) {
  return {
    type: Types.SET_LANGUAGE,
    payload: { lang, i18n, forceUpdate },
  };
}

// Other modules can listen to this action. (It's basically a notification.)
export function languageChanged(fromLang: string, toLang: string) {
  return {
    type: Types.LANGUAGE_CHANGED,
    payload: { fromLang, toLang },
  };
}
