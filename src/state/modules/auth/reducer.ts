import { Types } from './actions';
import { AnyAction } from 'redux';
import { AuthState } from './@types';

const initialState: AuthState = {
  token: null,
  user: null,
  loading: false,
};

export default (state = initialState, action: AnyAction) => {
  switch(action.type) {
    case Types.SIGN_IN:
    case Types.RETRIEVE_TOKEN_FROM_STORAGE:
      return {
        ...state,
        loading: true,
        token: null,
        user: null,
      };
    case Types.SIGN_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        token: action.payload.token,
        user: action.payload.user,
      };
    case Types.SIGN_IN_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case Types.RETRIEVE_TOKEN_SUCCESS:
    return {
      ...state,
      loading: false,
      token: action.payload.token,
    };
    case Types.RETRIEVE_TOKEN_FAILURE:
      return {
        ...state,
        loading: false,
        token: null,
      };
    case Types.REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        token: action.payload.token,
        user: action.payload.user,
      };
    case Types.SIGN_OUT:
      return initialState;
    default:
      return state;
  }
};
