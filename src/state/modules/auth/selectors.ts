import { ReduxStateType } from '../../rootReducer';

export const selectToken = (state: ReduxStateType) => state.auth.token;
export const selectAuthorization = (state: ReduxStateType) => selectToken(state) ? selectToken(state).token : null;
export const selectLoading = (state: ReduxStateType) => state.auth.loading;
