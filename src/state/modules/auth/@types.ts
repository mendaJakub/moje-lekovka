import { Token, User } from '../../../lib/ApiService/users/types';

export interface AuthState {
  token: Token;
  user: User;
  loading: boolean;
}
