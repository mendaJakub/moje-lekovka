import { takeLatest, call, put, select, fork } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import * as actions from './actions';
import { AnyAction } from 'redux';
import { AsyncStorage } from 'react-native';
import UsersApiService from '../../../lib/ApiService/users';
import { packSignInData, packSignUpData } from './transforms';
import { authStorageKeys } from './constants';
import { selectAuthorization } from './selectors';
import MyApiService from '../../../lib/ApiService/my';
import { requireNetConnection } from '../../../utils/connectionCheck';
import { handleError } from '../../../utils/errorHandling';
import { Toast } from 'native-base';
import i18n from 'i18next';
import { stopSubmit } from 'redux-form';
import { FORM_NAME as SIGN_IN_FORM } from '../../../view/forms/SignInForm';
import { FORM_NAME as SIGN_UP_FORM } from '../../../view/forms/SignUpForm';
import { FORM_NAME as RESET_PASSWORD_FORM } from '../../../view/forms/ResetPasswordForm';

function* watchSignIn() {
  yield takeLatest(actions.signIn.REQUEST, function* handle(action: AnyAction) {
    try {
      const data = packSignInData(action.payload);

      yield requireNetConnection();

      const response = yield call(UsersApiService.signIn, data);
      yield AsyncStorage.setItem(authStorageKeys.TOKEN, JSON.stringify(response.token));
      yield put(actions.signInSuccess(response.token, response.user));
    } catch(e) {
      yield put(stopSubmit(SIGN_IN_FORM, { _error: i18n.t('flashes.signIn.defaultFailure') }));
      switch(e.status) {
        case 404:
          Toast.show({
            text: i18n.t('flashes.signIn.404'),
            type: 'danger',
            buttonText: i18n.t('flashes.ok'),
            duration: 5000,
          });
          break;
        case 401:
          Toast.show({
            text: i18n.t('flashes.signIn.401'),
            type: 'danger',
            buttonText: i18n.t('flashes.ok'),
            duration: 5000,
          });
          break;
        default:
          handleError(e, i18n.t('flashes.signIn.defaultFailure'));
      }
    }
  });
}

function* watchSignUp() {
  yield takeLatest(actions.signUp.REQUEST, function* handle(action: AnyAction) {
    try {
      const data = packSignUpData(action.payload);

      yield requireNetConnection();

      yield call(UsersApiService.postUser, data);
      yield put(actions.signUp.success());
    } catch(e) {
      switch(e.status) {
        case 409:
          Toast.show({
            text: i18n.t('flashes.signUp.409'),
            type: 'danger',
            buttonText: i18n.t('flashes.ok'),
            duration: 5000,
          });
          break;
        default:
          handleError(e, i18n.t('flashes.signUp.defaultFailure'));
      }
      yield put(stopSubmit(SIGN_UP_FORM, { _error: i18n.t('flashes.signUp.defaultFailure') }));
    }
  });
}

function* watchRetrieveToken() {
  yield takeLatest(actions.Types.RETRIEVE_TOKEN_FROM_STORAGE, function* handle() {
    try {
      const token = yield AsyncStorage.getItem(authStorageKeys.TOKEN);
      if (token) {
        yield put(actions.retrieveTokenSuccess(JSON.parse(token)));
      } else {
        yield put(actions.retrieveTokenFailure());
      }
    } catch(e) {
      yield put(actions.retrieveTokenFailure());
    }
  });
}

function* watchRefreshToken() {
  yield takeLatest(actions.Types.REFRESH_TOKEN, function* handle() {
    try {
      const token = yield select(selectAuthorization);
      yield requireNetConnection();

      const response = yield call(MyApiService.getMyNewToken, token);
      if (response && response.token) {
        yield AsyncStorage.setItem(authStorageKeys.TOKEN, JSON.stringify(response.token));
        yield put(actions.refreshTokenSuccess(response.token, response.user));
      }
    } catch(e) {
      handleError(e, i18n.t('flashes.refreshToken.defaultFailure'));
      yield put(actions.refreshTokenFailure());
    }
  });
}

function* watchSignOut() {
  yield takeLatest(actions.Types.SIGN_OUT, function* handle() {
    try {
      yield AsyncStorage.removeItem(authStorageKeys.TOKEN);
    } catch(e) {
      console.warn('failed to remove token', e);
    }
  });
}

function* watchResetPassword() {
  yield takeLatest(actions.resetPassword.REQUEST, function* handle(action: AnyAction) {
    try {
      const data = action.payload;

      yield requireNetConnection();

      yield call(UsersApiService.resetPassword, data);
      yield put(actions.resetPasswordSuccess());
      Toast.show({
        text: i18n.t('flashes.resetPassword.success'),
        type: 'success',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
    } catch(e) {
      yield put(stopSubmit(RESET_PASSWORD_FORM, { _error: i18n.t('flashes.resetPassword.defaultFailure') }));
      handleError(e, i18n.t('flashes.resetPassword.defaultFailure'));
    }
  });
}

export default function* authFlow(): SagaIterator {
  yield fork(watchSignIn);
  yield fork(watchSignUp);
  yield fork(watchRetrieveToken);
  yield fork(watchRefreshToken);
  yield fork(watchSignOut);
  yield fork(watchResetPassword);
}
