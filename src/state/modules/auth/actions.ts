import { AnyAction } from 'redux';
import { createFormAction } from 'redux-form-saga';
import { User } from '../../../lib/ApiService/users/types';

export enum Types {
  SIGN_IN = 'auth/SIGN_IN',
  SIGN_IN_SUCCESS = 'auth/SIGN_IN_SUCCESS',
  SIGN_IN_FAILURE = 'auth/SIGN_IN_FAILURE',
  SIGN_UP = 'auth/SIGN_UP',
  SIGN_UP_FAILURE = 'auth/SIGN_UP_FAILURE',
  SIGN_OUT = 'auth/SIGN_OUT',
  REFRESH_TOKEN = 'auth/REFRESH_TOKEN',
  REFRESH_TOKEN_SUCCESS = 'auth/REFRESH_TOKEN_SUCCESS',
  REFRESH_TOKEN_FAILURE = 'auth/REFRESH_TOKEN_FAILURE',
  RETRIEVE_TOKEN_FROM_STORAGE = 'auth/RETRIEVE_TOKEN_FROM_STORAGE',
  RETRIEVE_TOKEN_SUCCESS = 'auth/RETRIEVE_TOKEN_SUCCESS',
  RETRIEVE_TOKEN_FAILURE = 'auth/RETRIEVE_TOKEN_FAILURE',
  RESET_PASSWORD = 'auth/RESET_PASSWORD',
  RESET_PASSWORD_SUCCESS = 'auth/RESET_PASSWORD_SUCCESS',
  RESET_PASSWORD_FAILURE = 'auth/RESET_PASSWORD_FAILURE',
}

export const signUp = createFormAction(Types.SIGN_UP);
export const signIn = createFormAction(Types.SIGN_IN);
export const resetPassword = createFormAction(Types.RESET_PASSWORD);

export function signInSuccess(token: string, user?: User): AnyAction {
  return {
    type: Types.SIGN_IN_SUCCESS,
    payload: { token, user },
  };
}

export function signInFailure(): AnyAction {
  return {
    type: Types.SIGN_IN_FAILURE,
  };
}

export function signUpFailure(): AnyAction {
  return {
    type: Types.SIGN_UP_FAILURE,
  };
}

export function refreshToken() {
  return {
    type: Types.REFRESH_TOKEN,
  };
}

export function refreshTokenSuccess(token: string, user: User) {
  return {
    type: Types.REFRESH_TOKEN_SUCCESS,
    payload: { token, user },
  };
}

export function refreshTokenFailure() {
  return {
    type: Types.REFRESH_TOKEN_FAILURE,
  };
}

export function signOut(): AnyAction {
  return {
    type: Types.SIGN_OUT,
  };
}

export function retrieveTokenFromStorage(): AnyAction {
  return {
    type: Types.RETRIEVE_TOKEN_FROM_STORAGE,
  };
}

export function retrieveTokenSuccess(token: string): AnyAction {
  return {
    type: Types.RETRIEVE_TOKEN_SUCCESS,
    payload: { token },
  };
}

export function retrieveTokenFailure(): AnyAction {
  return {
    type: Types.RETRIEVE_TOKEN_FAILURE,
  };
}

export function resetPasswordSuccess(): AnyAction {
  return {
    type: Types.RESET_PASSWORD_SUCCESS,
  };
}

export function resetPasswordFailure(): AnyAction {
  return {
    type: Types.RESET_PASSWORD_FAILURE,
  };
}
