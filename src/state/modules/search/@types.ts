import { ModuleType } from '../../types';
import {
  MedicationPreview,
  Medication,
} from '../../../lib/ApiService/medication/types';

export interface SearchState {
  search: ModuleType<MedicationPreview[]>;
  detail: ModuleType<Medication>;
}
