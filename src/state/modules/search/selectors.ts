import { ReduxStateType } from '../../rootReducer';
import _ from 'lodash';

const selectSearchRoot = (state: ReduxStateType) => state.search;
const selectSearchResultsRoot = (state: ReduxStateType) => selectSearchRoot(state).search;
const selectSearchDetailRoot = (state: ReduxStateType) => selectSearchRoot(state).detail;

export const selectSearchError = (state: ReduxStateType) => _.get(selectSearchResultsRoot(state), 'error');
export const selectSearchLoading = (state: ReduxStateType) => _.get(selectSearchResultsRoot(state), 'loading');
export const selectSearchResults = (state: ReduxStateType) => _.get(selectSearchResultsRoot(state), 'payload');

export const selectSearchDetailError = (state: ReduxStateType) => _.get(selectSearchDetailRoot(state), 'error');
export const selectSearchDetailLoading = (state: ReduxStateType) => _.get(selectSearchDetailRoot(state), 'loading');
export const selectSearchDetail = (state: ReduxStateType) => _.get(selectSearchDetailRoot(state), 'payload');
