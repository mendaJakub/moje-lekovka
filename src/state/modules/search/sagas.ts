import { takeLatest, call, put, select, fork } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import * as actions from './actions';
import { selectAuthorization } from '../auth/selectors';
import MedicationApiService from '../../../lib/ApiService/medication';
import { handleError } from '../../../utils/errorHandling';
import i18n from 'i18next';
import { AnyAction } from 'redux';

function* watchSearchForMedication() {
  yield takeLatest(actions.Types.SEARCH_FOR_MEDICATION, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);

      const response = yield call(MedicationApiService.searchForMedication, token, action.payload.search);

      yield put(actions.searchForMedicationSuccess(response));
    } catch (e) {
      handleError(e, i18n.t('flashes.searchForMedication.defaultFailure'));
      yield put(actions.searchForMedicationFailure(e));
    }
  });
}

function* watchGetMedicationDetail() {
  yield takeLatest(actions.Types.GET_MEDICATION_DETAIL, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);

      const response = yield call(MedicationApiService.getMedicationDetail, token, action.payload.id);

      yield put(actions.getMedicationDetailSuccess(response));
    } catch (e) {
      handleError(e, i18n.t('flashes.getMedicationDetail.defaultFailure'));
      yield put(actions.getMedicationDetailFailure(e));
    }
  });
}

export default function* searchFlow(): SagaIterator {
  yield fork(watchSearchForMedication);
  yield fork(watchGetMedicationDetail);
}
