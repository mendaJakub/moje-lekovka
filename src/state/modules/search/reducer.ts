import { Types } from './actions';
import { AnyAction } from 'redux';
import { SearchState } from './@types';
import { emptyModule } from '../../constants';

const initialState: SearchState= {
  search: { ...emptyModule },
  detail: { ...emptyModule },
};

export default (state = initialState, action: AnyAction) => {
  switch(action.type) {
    case Types.SEARCH_FOR_MEDICATION:
      return {
        ...state,
        search: {
          ...state.search,
          loading: true,
          payload: null,
          error: null,
        },
      };
    case Types.SEARCH_FOR_MEDICATION_SUCCESS:
      return {
        ...state,
        search: {
          ...state.search,
          loading: false,
          payload: action.payload.data,
          error: null,
        },
      };
    case Types.SEARCH_FOR_MEDICATION_FAILURE:
      return {
        ...state,
        search: {
          ...state.search,
          loading: false,
          payload: null,
          error: action.payload.error,
        },
      };
    case Types.GET_MEDICATION_DETAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: true,
          payload: null,
          error: null,
        },
      };
    case Types.GET_MEDICATION_DETAIL_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          payload: action.payload.data,
          error: null,
        },
      };
    case Types.GET_MEDICATION_DETAIL_FAILURE:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          payload: null,
          error: action.payload.error,
        },
      };
    case Types.CLEAR_SEARCH_STATE:
      return initialState;
    default:
      return state;
  }
};
