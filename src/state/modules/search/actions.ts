import { AnyAction } from 'redux';
import { createFormAction } from 'redux-form-saga';
import { MedicationPreview } from '../../../lib/ApiService/medication/types';
import MedicationDetail from '../../../view/screens/MedicationDetail';

export enum Types {
  SEARCH_FOR_MEDICATION = 'search/SEARCH_FOR_MEDICATION',
  SEARCH_FOR_MEDICATION_SUCCESS = 'search/SEARCH_FOR_MEDICATION_SUCCESS',
  SEARCH_FOR_MEDICATION_FAILURE = 'search/SEARCH_FOR_MEDICATION_FAILURE',
  GET_MEDICATION_DETAIL = 'search/GET_MEDICATION_DETAIL',
  GET_MEDICATION_DETAIL_SUCCESS = 'search/GET_MEDICATION_DETAIL_SUCCESS',
  GET_MEDICATION_DETAIL_FAILURE = 'search/GET_MEDICATION_DETAIL_FAILURE',
  CLEAR_SEARCH_STATE = 'search/CLEAR_SEARCH_STATE',
}

export function searchForMedication(search: string): AnyAction {
  return {
    type: Types.SEARCH_FOR_MEDICATION,
    payload: { search },
  };
}

export function searchForMedicationSuccess(data: MedicationPreview[]): AnyAction {
  return {
    type: Types.SEARCH_FOR_MEDICATION_SUCCESS,
    payload: { data },
  };
}

export function searchForMedicationFailure(error: Error): AnyAction {
  return {
    type: Types.SEARCH_FOR_MEDICATION_FAILURE,
    payload: { error },
  };
}

export function getMedicationDetail(id: string): AnyAction {
  return {
    type: Types.GET_MEDICATION_DETAIL,
    payload: { id },
  };
}

export function getMedicationDetailSuccess(data: MedicationDetail): AnyAction {
  return {
    type: Types.GET_MEDICATION_DETAIL_SUCCESS,
    payload: { data },
  };
}

export function getMedicationDetailFailure(error: Error): AnyAction {
  return {
    type: Types.GET_MEDICATION_DETAIL_FAILURE,
    payload: { error },
  };
}

export function clearSearchState(): AnyAction {
  return {
    type: Types.CLEAR_SEARCH_STATE,
  };
}
