import { ModuleType } from '../../types';
import { MedicationTakingDetail, MedicationTakingPreview } from '../../../lib/ApiService/medication/types';

export interface MedicationTakingItem extends Partial<MedicationTakingPreview> {
  detail: ModuleType<MedicationTakingDetail>;
}

export interface MyState {
  medication: ModuleType<MedicationTakingItem[]>;
}
