import { Types } from './actions';
import { AnyAction } from 'redux';
import { MedicationTakingItem, MyState } from './@types';
import { emptyModule } from '../../constants';
import _ from 'lodash';

const initialState: MyState = {
  medication: {
    ...emptyModule,
  },
};

export default (state = initialState, action: AnyAction) => {
  switch(action.type) {
    case Types.GET_MY_MEDICATION:
      return {
        ...state,
        medication: {
          ...state.medication,
          loading: true,
          error: null,
        },
      };
    case Types.GET_MY_MEDICATION_SUCCESS:
      const medList = action.payload.medicationList.map((medication: MedicationTakingItem) => {
        const localCopy = (state.medication.payload || []).find((m: MedicationTakingItem) => m._id === medication._id);

        if (localCopy) {
          return {
            ...localCopy,
            medication,
          };
        }
        return medication;
      });

      return {
        ...state,
        medication: {
          ...state.medication,
          loading: false,
          payload: medList,
          error: null,
        },
      };
    case Types.GET_MY_MEDICATION_FAILURE:
      return {
        ...state,
        medication: {
          ...state.medication,
          loading: false,
          error: action.payload.error,
        },
      };
    case Types.GET_MY_MEDICATION_DETAIL:
      let newMedicationList: MedicationTakingItem[] = [];
      if (!state.medication.payload) {
        newMedicationList.push({
          _id: action.payload.id,
          detail: {
            loading: true,
            error: null,
            payload: null,
          },
        });
      } else {
        newMedicationList = state.medication.payload.map((m: MedicationTakingItem) => {
          if (m._id === action.payload.id) {
            return {
              ...m,
              detail: {
                loading: true,
                error: null,
                payload: null,
              },
            };
          }
          return m;
        });
      }
      return {
        ...state,
        medication: {
          ...state.medication,
          payload: newMedicationList,
        },
      };
    case Types.GET_MY_MEDICATION_DETAIL_SUCCESS:
      let newMedList: MedicationTakingItem[] = [];
      if (!state.medication.payload) {
        newMedList.push({
          _id: action.payload.id,
          userId: action.payload.data.userId,
          title: action.payload.data.title,
          alarms: action.payload.data.alarms,
          detail: {
            loading: false,
            error: null,
            payload: action.payload.data.medicationInfo || _.get(action, 'payload.data.detail.payload'),
          },
        });
      } else {
        newMedList = state.medication.payload.map((m: MedicationTakingItem) => {
          if (m._id === action.payload.id) {
            return {
              ...m,
              userId: action.payload.data.userId,
              title: action.payload.data.title,
              alarms: action.payload.data.alarms,
              detail: {
                loading: false,
                error: null,
                payload: action.payload.data.medicationInfo || _.get(action, 'payload.data.detail.payload'),
              },
            };
          }
          return m;
        });
      }
      return {
        ...state,
        medication: {
          ...state.medication,
          payload: newMedList,
        },
      };
    case Types.GET_MY_MEDICATION_DETAIL_FAILURE:
      let newList: MedicationTakingItem[] = [];
      if (!state.medication.payload) {
        newList.push({
          _id: action.payload.id,
          detail: {
            loading: false,
            error: action.payload.error,
            payload: null,
          },
        });
      } else {
        newList = state.medication.payload.map((m: MedicationTakingItem) => {
          if (m._id === action.payload.id) {
            return {
              ...m,
              detail: {
                ...m.detail,
                loading: false,
                error: action.payload.error,
              },
            };
          }
          return m;
        });
      }
      return {
        ...state,
        medication: {
          ...state.medication,
          payload: newList,
        },
      };
    default:
      return state;
  }
};
