import { ReduxStateType } from '../../rootReducer';
import _ from 'lodash';
import { MedicationTakingItem } from './@types';
import { formValueSelector } from 'redux-form';
import { FORM_NAME } from '../../../view/forms/AlarmsEditingForm';

const selectMyMedicationRoot = (state: ReduxStateType) => state.my.medication;
export const selectMyMedicationLoading = (state: ReduxStateType) => _.get(selectMyMedicationRoot(state), 'loading');
export const selectMyMedicationError = (state: ReduxStateType) => _.get(selectMyMedicationRoot(state), 'error');
export const selectMyMedication = (state: ReduxStateType) => _.get(selectMyMedicationRoot(state), 'payload');

export const selectMyMedicationDetailRoot = (id: string) => (state: ReduxStateType) => {
  const medicationPayload = selectMyMedication(state);

  if (!medicationPayload) {
    return null;
  }
  return medicationPayload.find((m: MedicationTakingItem) => m._id === id);
};
export const selectMyMedicationDetailLoading = (id: string) => (state: ReduxStateType) => _.get(selectMyMedicationDetailRoot(id)(state), 'detail.loading');
export const selectMyMedicationDetailError = (id: string) => (state: ReduxStateType) => _.get(selectMyMedicationDetailRoot(id)(state), 'detail.error');
export const selectMyMedicationDetail = (id: string) => (state: ReduxStateType) => _.get(selectMyMedicationDetailRoot(id)(state), 'detail.payload');

const alarmsEditingFormSelector = formValueSelector(FORM_NAME);
export const selectItemField = (state: ReduxStateType) => (item: string) => alarmsEditingFormSelector(state, item);
