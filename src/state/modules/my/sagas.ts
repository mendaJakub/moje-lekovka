import { takeLatest, call, put, select, fork, take, all } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import * as actions from './actions';
import { AsyncStorage } from 'react-native';
import { myStorageKeys } from './constants';
import { selectAuthorization } from '../auth/selectors';
import MyApiService from '../../../lib/ApiService/my';
import { checkNetConnection, requireNetConnection } from '../../../utils/connectionCheck';
import { handleError } from '../../../utils/errorHandling';
import i18n from 'i18next';
import { GetMyMedicationDetailResponse, GetMyMedicationResponse } from '../../../lib/ApiService/my/types';
import { selectMyMedication, selectMyMedicationDetailRoot } from './selectors';
import { AnyAction } from 'redux';
import { MedicationTakingItem } from './@types';
import { stopSubmit } from 'redux-form';
import NavigationService from '../../../utils/NavigationService';
import { Toast } from 'native-base';
import { FORM_NAME as NEW_MEDICATION_FORM } from '../../../view/forms/NewMedicationForm';
import { FORM_NAME as EDIT_ALARMS_FORM } from '../../../view/forms/AlarmsEditingForm';
import { Alarm } from '../../../lib/ApiService/medication/types';
import { cancelNotifications, setUpNotification } from '../../../utils/notifications';

function* watchGetMyMedication() {
  yield takeLatest(actions.Types.GET_MY_MEDICATION, function* handle() {
    try {
      const token = yield select(selectAuthorization);
      const isConnected = yield checkNetConnection();
      let response: GetMyMedicationResponse;

      if (isConnected) {
        response = yield call(MyApiService.getMyMedication, token);
      } else {
        const valFromAsyncStorage = yield AsyncStorage.getItem(myStorageKeys.MEDICATION);
        response = valFromAsyncStorage ? JSON.parse(valFromAsyncStorage) : [];
      }

      yield put(actions.getMyMedicationSuccess(response));
    } catch (e) {
      handleError(e, i18n.t('flashes.getMyMedication.defaultFailure'));
      yield put(actions.getMyMedicationFailure(e));
    }
  });
}

function* watchGetMyMedicationDetail() {
  yield takeLatest(actions.Types.GET_MY_MEDICATION_DETAIL, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);
      const isConnected = yield checkNetConnection();
      let response: GetMyMedicationDetailResponse;

      if (isConnected) {
        response = yield call(MyApiService.getMyMedicationDetail, token, action.payload.id);
      } else {
        const valFromAsyncStorage = yield AsyncStorage.getItem(myStorageKeys.MEDICATION);
        const list = valFromAsyncStorage ? JSON.parse(valFromAsyncStorage) : [];
        response = list.find((m: MedicationTakingItem) => m._id === action.payload.id);
      }

      yield put(actions.getMyMedicationDetailSuccess(action.payload.id, response));
    } catch (e) {
      handleError(e, i18n.t('flashes.getMyMedicationDetail.defaultFailure'));
      yield put(actions.getMyMedicationDetailFailure(action.payload.id, e));
    }
  });
}

function* watchGetMyMedicationSuccess() {
  yield takeLatest(actions.Types.GET_MY_MEDICATION_SUCCESS, function* handle() {
    try {
      const medication = yield select(selectMyMedication);

      yield AsyncStorage.setItem(myStorageKeys.MEDICATION, JSON.stringify(medication));
    } catch (e) {
      console.warn('async storage failed to save medication');
    }
  });
}

function* watchGetMyMedicationDetailSuccess() {
  yield takeLatest(actions.Types.GET_MY_MEDICATION_DETAIL_SUCCESS, function* handle() {
    try {
      const medication = yield select(selectMyMedication);

      yield AsyncStorage.setItem(myStorageKeys.MEDICATION, JSON.stringify(medication));
    } catch (e) {
      console.warn('async storage failed to save medication');
    }
  });
}

function* watchAddMyMedication() {
  yield takeLatest(actions.addMyMedication.REQUEST, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);
      const data = action.payload;

      yield requireNetConnection();

      const response = yield call(MyApiService.postMyMedication, token, data);
      yield put(actions.addMyMedication.success(response));
      yield put(actions.getMyMedication());
      NavigationService.navigate('ALARMS_EDITING', { medicationTakingId: response._id });
    } catch(e) {
      handleError(e, i18n.t('flashes.addMyMedication.defaultFailure'));
      yield put(stopSubmit(NEW_MEDICATION_FORM, { _error: i18n.t('flashes.addMyMedication.defaultFailure') }));
    }
  });
}

function* watchEditMyMedication() {
  yield takeLatest(actions.editMyMedication.REQUEST, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);
      const { id, ...data } = action.payload;

      yield requireNetConnection();

      const response = yield call(MyApiService.putMyMedication, token, id, data);
      yield put(actions.editMyMedication.success(response));
      yield put(actions.getMyMedication());
      NavigationService.navigate('MEDICATION_LIST');
    } catch(e) {
      handleError(e, i18n.t('flashes.editMyMedication.defaultFailure'));
      yield put(stopSubmit(NEW_MEDICATION_FORM, { _error: i18n.t('flashes.editMyMedication.defaultFailure') }));
    }
  });
}

function* watchDeleteMyMedication() {
  yield takeLatest(actions.Types.DELETE_MY_MEDICATION, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);
      const { id } = action.payload;
      const medication = yield select(selectMyMedicationDetailRoot(id));

      yield requireNetConnection();

      yield all((medication.alarms || []).map((alarm: Alarm) => cancelNotifications(alarm.notificationIds)));

      yield call(MyApiService.deleteMyMedication, token, id);
      Toast.show({
        text: i18n.t('flashes.deleteMyMedication.success'),
        type: 'success',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
      yield put(actions.deleteMyMedicationSuccess(id));
      yield put(actions.getMyMedication());
      NavigationService.navigate('MEDICATION_LIST');
    } catch(e) {
      handleError(e, i18n.t('flashes.deleteMyMedication.defaultFailure'));
      yield put(actions.deleteMyMedicationFailure(e));
    }
  });
}

function* watchEditMyAlarms() {
  yield takeLatest(actions.editMyAlarms.REQUEST, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);
      const { medicationId, ...data } = action.payload;
      const medication = yield select(selectMyMedicationDetailRoot(medicationId));

      yield requireNetConnection();

      const alarmsWithNotifications = yield all(
        data.alarms.map((alarm: Alarm) => setUpNotification(medication.title, alarm))
      );

      yield call(MyApiService.putMyMedicationAlarms, token, medicationId, { ...data, alarms: alarmsWithNotifications });
      yield put(actions.getMyMedicationDetail(medicationId));

      const resultAction = yield take([actions.Types.GET_MY_MEDICATION_DETAIL_SUCCESS]);
      if (resultAction && resultAction.type === actions.Types.GET_MY_MEDICATION_DETAIL_SUCCESS) {
        yield put(actions.editMyAlarms.success());
        NavigationService.navigate('MEDICATION_DETAIL', { medicationTakingId: medicationId });
      }
    } catch(e) {
      handleError(e, i18n.t('flashes.editMyAlarms.defaultFailure'));
      yield put(stopSubmit(EDIT_ALARMS_FORM, { _error: i18n.t('flashes.editMyAlarms.defaultFailure') }));
    }
  });
}

function* watchDeleteMyMedicationAlarm() {
  yield takeLatest(actions.Types.DELETE_MY_MEDICATION_ALARM, function* handle(action: AnyAction) {
    try {
      const token = yield select(selectAuthorization);
      const { medicationId, alarmId } = action.payload;
      const medication = yield select(selectMyMedicationDetailRoot(medicationId));
      const alarmToDelete = (medication.alarms || []).find((alarm: Alarm) => alarm._id === alarmId);

      yield requireNetConnection();

      if (alarmToDelete) {
        yield cancelNotifications(alarmToDelete.notificationIds);
      }

      yield call(MyApiService.deleteMyMedicationAlarm, token, medicationId, alarmId);
      Toast.show({
        text: i18n.t('flashes.deleteMyMedicationAlarm.success'),
        type: 'success',
        buttonText: i18n.t('flashes.ok'),
        duration: 5000,
      });
      yield put(actions.getMyMedicationDetail(medicationId));
      const resultAction = yield take([actions.Types.GET_MY_MEDICATION_DETAIL_SUCCESS]);
      if (resultAction && resultAction.type === actions.Types.GET_MY_MEDICATION_DETAIL_SUCCESS) {
        yield put(actions.deleteMyMedicationAlarmSuccess(medicationId, alarmId));
      }
    } catch(e) {
      handleError(e, i18n.t('flashes.deleteMyMedicationAlarm.defaultFailure'));
      yield put(actions.deleteMyMedicationAlarmFailure(e));
    }
  });
}

export default function* myFlow(): SagaIterator {
  yield fork(watchGetMyMedication);
  yield fork(watchGetMyMedicationDetail);
  yield fork(watchGetMyMedicationSuccess);
  yield fork(watchGetMyMedicationDetailSuccess);
  yield fork(watchAddMyMedication);
  yield fork(watchEditMyMedication);
  yield fork(watchDeleteMyMedication);
  yield fork(watchEditMyAlarms);
  yield fork(watchDeleteMyMedicationAlarm);
}
