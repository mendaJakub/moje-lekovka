import { AnyAction } from 'redux';
import { createFormAction } from 'redux-form-saga';
import { Medication, MedicationTakingDetail, MedicationTakingPreview } from '../../../lib/ApiService/medication/types';

export enum Types {
  GET_MY_MEDICATION = 'my/GET_MEDICATION',
  GET_MY_MEDICATION_SUCCESS = 'my/GET_MEDICATION_SUCCESS',
  GET_MY_MEDICATION_FAILURE = 'my/GET_MEDICATION_FAILURE',
  GET_MY_MEDICATION_DETAIL = 'my/GET_MEDICATION_DETAIL',
  GET_MY_MEDICATION_DETAIL_SUCCESS = 'my/GET_MEDICATION_DETAIL_SUCCESS',
  GET_MY_MEDICATION_DETAIL_FAILURE = 'my/GET_MEDICATION_DETAIL_FAILURE',
  ADD_MY_MEDICATION = 'my/ADD_MY_MEDICATION',
  ADD_MY_MEDICATION_SUCCESS = 'my/ADD_MY_MEDICATION_SUCCESS',
  ADD_MY_MEDICATION_FAILURE = 'my/ADD_MY_MEDICATION_FAILURE',
  EDIT_MY_MEDICATION = 'my/EDIT_MY_MEDICATION',
  EDIT_MY_MEDICATION_SUCCESS = 'my/EDIT_MY_MEDICATION_SUCCESS',
  EDIT_MY_MEDICATION_FAILURE = 'my/EDIT_MY_MEDICATION_FAILURE',
  DELETE_MY_MEDICATION = 'my/DELETE_MY_MEDICATION',
  DELETE_MY_MEDICATION_SUCCESS = 'my/DELETE_MY_MEDICATION_SUCCESS',
  DELETE_MY_MEDICATION_FAILURE = 'my/DELETE_MY_MEDICATION_FAILURE',
  EDIT_MY_ALARMS = 'my/EDIT_MY_ALARMS',
  EDIT_MY_ALARMS_SUCCESS = 'my/EDIT_MY_ALARMS_SUCCESS',
  EDIT_MY_ALARMS_FAILURE = 'my/EDIT_MY_ALARMS_FAILURE',
  DELETE_MY_MEDICATION_ALARM = 'my/DELETE_MY_MEDICATION_ALARM',
  DELETE_MY_MEDICATION_ALARM_SUCCESS = 'my/DELETE_MY_MEDICATION_ALARM_SUCCESS',
  DELETE_MY_MEDICATION_ALARM_FAILURE = 'my/DELETE_MY_MEDICATION_ALARM_FAILURE',
}

export const addMyMedication = createFormAction(Types.ADD_MY_MEDICATION);
export const editMyMedication = createFormAction(Types.EDIT_MY_MEDICATION);
export const editMyAlarms = createFormAction(Types.EDIT_MY_ALARMS);

export function getMyMedication(): AnyAction {
  return {
    type: Types.GET_MY_MEDICATION,
  };
}

export function getMyMedicationSuccess(medicationList: MedicationTakingPreview[]): AnyAction {
  return {
    type: Types.GET_MY_MEDICATION_SUCCESS,
    payload: { medicationList },
  };
}

export function getMyMedicationFailure(error: Error): AnyAction {
  return {
    type: Types.GET_MY_MEDICATION_FAILURE,
    payload: { error },
  };
}

export function getMyMedicationDetail(id: string): AnyAction {
  return {
    type: Types.GET_MY_MEDICATION_DETAIL,
    payload: { id },
  };
}

export function getMyMedicationDetailSuccess(id: string, data: MedicationTakingDetail): AnyAction {
  return {
    type: Types.GET_MY_MEDICATION_DETAIL_SUCCESS,
    payload: { id, data },
  };
}

export function getMyMedicationDetailFailure(id: string, error: Error): AnyAction {
  return {
    type: Types.GET_MY_MEDICATION_DETAIL_FAILURE,
    payload: { error },
  };
}

export function addMyMedicationSuccess(medication: MedicationTakingDetail) {
  return {
    type: Types.ADD_MY_MEDICATION_SUCCESS,
    payload: { medication },
  };
}

export function addMyMedicationFailure(error: Error) {
  return {
    type: Types.ADD_MY_MEDICATION_FAILURE,
    payload: { error },
  };
}

export function editMyMedicationSuccess(medication: MedicationTakingDetail) {
  return {
    type: Types.EDIT_MY_MEDICATION_SUCCESS,
    payload: { medication },
  };
}

export function editMyMedicationFailure(error: Error) {
  return {
    type: Types.EDIT_MY_MEDICATION_FAILURE,
    payload: { error },
  };
}

export function deleteMyMedication(id: string) {
  return {
    type: Types.DELETE_MY_MEDICATION,
    payload: { id },
  };
}

export function deleteMyMedicationSuccess(id: string) {
  return {
    type: Types.DELETE_MY_MEDICATION_SUCCESS,
    payload: { id },
  };
}

export function deleteMyMedicationFailure(error: Error) {
  return {
    type: Types.DELETE_MY_MEDICATION_FAILURE,
    payload: { error },
  };
}

export function editMyAlarmsSuccess() {
  return {
    type: Types.EDIT_MY_ALARMS_SUCCESS,
  };
}

export function editMyAlarmsFailure(error: Error) {
  return {
    type: Types.EDIT_MY_ALARMS_FAILURE,
    payload: { error },
  };
}

export function deleteMyMedicationAlarm(medicationId: string, alarmId: string) {
  return {
    type: Types.DELETE_MY_MEDICATION_ALARM,
    payload: { medicationId, alarmId },
  };
}

export function deleteMyMedicationAlarmSuccess(medicationId: string, alarmId: string) {
  return {
    type: Types.DELETE_MY_MEDICATION_ALARM_SUCCESS,
    payload: { medicationId, alarmId },
  };
}

export function deleteMyMedicationAlarmFailure(error: Error) {
  return {
    type: Types.DELETE_MY_MEDICATION_ALARM_FAILURE,
    payload: { error },
  };
}
