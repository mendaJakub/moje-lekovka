export interface ModuleType<Payload> {
  loading: boolean;
  error: Error;
  payload: Payload;
}
