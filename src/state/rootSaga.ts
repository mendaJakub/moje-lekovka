import formActionSaga from 'redux-form-saga';
import authSaga from './modules/auth/sagas';
import mySaga from './modules/my/sagas';
import languageSaga from './modules/language/sagas';
import searchSaga from './modules/search/sagas';

export default [formActionSaga, authSaga, languageSaga, mySaga, searchSaga];
