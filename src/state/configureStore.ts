import { applyMiddleware, createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import createSagaMiddleware, { Saga } from 'redux-saga';

import rootReducers from './rootReducer';
import rootSagas from './rootSaga';

const sagaMiddleware = createSagaMiddleware();

// FIXME include logger only in deployment environment
const middlewares = [sagaMiddleware, createLogger({ collapsed: true })];

const configureStore = (initialState: object = {}) => {
  const rootReducer = combineReducers(rootReducers);

  const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middlewares)));

  rootSagas.forEach((saga: Saga) => {
    sagaMiddleware.run(saga);
  });

  return { store };
};

export default configureStore;
