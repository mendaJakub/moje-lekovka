import { Constants } from 'expo';
import devConf from './development';
import prodConf from './production';

export const getEnv = (env = Constants.manifest.releaseChannel) => {
  if (__DEV__) {
    return devConf;
  } else {
    return prodConf;
  }
};
